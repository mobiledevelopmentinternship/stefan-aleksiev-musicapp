//
//  UIViewController+Helpers.swift
//  music-app
//
//  Created by Stefan Aleksiev on 18.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message:String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: Constants.Alert.actionTitle, style: .default)
            alert.addAction(ok)
            self.present(alert, animated: true)
        }
    }
    
    func showEmptyDataAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: Constants.Alert.actionTitle, style: .default) {
                UIAlertAction in
                self.navigationController?.popViewController(animated: true)
            }
            alert.addAction(okAction)
            self.present(alert, animated: true)
        }
    }
    
    func instantiateViewController<T>(with identifier: String) -> T where T: UIViewController {
        let storyboard : UIStoryboard = UIStoryboard(name: identifier, bundle: nil)
        let newVC: T = storyboard.instantiateViewController(withIdentifier: identifier) as! T
        return newVC
    }
}
