//
//  Array+Helpers.swift
//  music-app
//
//  Created by Stefan Aleksiev on 20.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

extension Array {
    func choose(_ n: Int) -> Array { return Array(shuffled().prefix(n)) }
}
