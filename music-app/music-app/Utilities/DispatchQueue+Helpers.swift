//
//  DispatchQueue+Helpers.swift
//  music-app
//
//  Created by Stefan Aleksiev on 16.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

extension DispatchQueue {
    static func background(background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    completion()
                })
            }
        }
    }
    
}
