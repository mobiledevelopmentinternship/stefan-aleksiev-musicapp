//
//  UIImage+Helpers.swift
//  music-app
//
//  Created by Stefan Aleksiev on 1.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

extension UIImageView {
    func maskWith(image: UIImageView) {
        let maskImageView = UIImageView()
        maskImageView.image = #imageLiteral(resourceName: "Mask-1")
        maskImageView.frame = image.bounds
        image.mask = maskImageView
    }
}
