//
//  SearchBar-Helpers.swift
//  music-app
//
//  Created by Stefan Aleksiev on 3.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

extension UISearchBar {
    func setBackground() {
        if let textfield = self.value(forKey: "searchField") as? UITextField {
            textfield.textColor = UIColor.blue
            
            if let backgroundview = textfield.subviews.first {
                backgroundview.backgroundColor = UIColor.white
                backgroundview.layer.cornerRadius = CGFloat(Constants.SearchBar.cornerRadius)
                backgroundview.clipsToBounds = true
            }
        }
    }
}

