//
//  UIColor+Helpers.swift
//  music-app
//
//  Created by Stefan Aleksiev on 10.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

extension UIColor {
    static let appPurple = UIColor(red:0.53, green:0.24, blue:0.78, alpha:1.0)
}
