//
//  Constants.swift
//  music-app
//
//  Created by Stefan Aleksiev on 23.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import UIKit

enum Constants {
    enum TableView {
        static let numberOfSections = 1
    }
    
    enum Alert {
        static let actionTitle = "OK"
        static let favouritesTitle = "Go to ❤️"
    }
    
    enum Titles {
        static let top10artistsLabel = "Popular artists"
        static let top10albumsLabel = "Editor's choice"
        static let seeAllBtnLabel = "see all"
        static let popularSongsLabel = "Popular Songs"
        static let recommendedArtistsLabel = "Recommended"
        static let recentAlbumsLabel = "Recent Albums"
    }
    
    enum PlaylistStyle {
        static let borderWidth = 0
        static let shadowOpacity = 5
        static let shadowRadius = 10
    }
    
    enum SearchBar {
        static let artistPlaceholder = "Search for any artist by its name."
        static let albumPlaceholder = "Search for any album by its name."
        static let songPlaceholder = "Search for any song by its name."
        static let favouritesPlaceholder = "Search..."
        static let cornerRadius = 10
    }
    
    enum NavigationItem {
        static let size = CGRect(origin: CGPoint.zero, size: CGSize(width: 50, height: 50))
    }
    
    enum NavigationTitles {
        static let homeTitle = "Home"
        static let favouritesTitle = "Favourites"
    }
    
    enum Labels {
        static let noDataInFavourites = "There is nothing saved yet in your favourites."
        static let typeError = "There is no such type"
        static let noPrevious = "There is no previous element."
        static let noNext = "There is no next element."
    }
    
    enum ImagePath {
        static let plusIcon = "plus-icon.png"
        static let favouriteIcon = "favourites-icon.png"
        static let backIcon = "back-button-icon.png"
        static let defaultBackground = "BG.png"
        static let trashIcon = "trash-icon"
        static let defaultTrack = "default-track-image.png"
        
        enum SectionImages {
            static let playlist = "playlist-icon.png"
            static let artist = "artist-icon.png"
            static let album = "album-icon.png"
            static let track = "track-icon.png"
        }
        
        enum MediaPlayerIcon {
            static let play = "play-song-icon.png"
            static let stop = "pause-song-icon"
        }
    }
    
    enum Entities {
        static let playlist = "PlaylistEntity"
        static let artist = "ArtistEntity"
        static let album = "AlbumEntity"
        static let track = "TrackEntity"
        
        enum value {
            static let id = "id"
            static let type = "type"
            static let title = "title"
            static let picture = "picture"
            static let preview = "preview"
            static let duration = "duration"
            static let tracklist = "tracklist"
        }
    }
    
    enum SectionHeaderTitles {
        static let playlist = "Playlist"
        static let artists = "Artists"
        static let albums = "Albums"
        static let tracks = "Tracks"
    }
    
    enum saveMessageResponse {
        static let success = "Saved sucessfully in your favourites.\n\nNavigate with the ❤️ on the right corner, if you want to check them."
        static let failure = "Already exists in your favourites.\n\nNavigate with the ❤️ on the right corner, if you want to check it"
        enum saveTitleResponse {
            static let success = "Success"
            static let failure = "Failure"
        }
    }
}
