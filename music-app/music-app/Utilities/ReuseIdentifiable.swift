//
//  ReuseIdentifiable.swift
//  music-app
//
//  Created by Stefan Aleksiev on 7.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

protocol ReuseIdentifiable {
    static var reuseIdentifier: String { get }
}

extension ReuseIdentifiable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: ReuseIdentifiable {}
extension UICollectionViewCell: ReuseIdentifiable {}
extension UIViewController: ReuseIdentifiable {}
extension UITableViewHeaderFooterView: ReuseIdentifiable {}
