# ****Music App**** 🔉

Similar to one of the most famous apps - ****Spotify**** ![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Spotify_logo_without_text.svg/1024px-Spotify_logo_without_text.svg.png =20x20), music app allows its user to be able to see latest and most popular charts of artists and albums.Furthermore , the end-user can search for any artist and his/her specific tracks and albums.In addition, there is a media player which helps to play a preview of all available songs.In conclusion , all your favourites tracks , songs , albums or playlist could be saved in your favourites , so you can come back to them later.

## Architecture & Design Patterns

### Architecture

##### - Model-View-Controller (MVC)

MVC is undoubtedly the most-used design pattern of all. It classifies objects according to their general role in your application and encourages clean separation of code based on role.

It all comes down to code separation and reusability. Ideally, the View should be completely separated from the Model. If the View doesn’t rely on a specific implementation of the Model, then it can be reused with a different model to present some other data.

### Design Patterns

#### - Singleton

The Singleton design  ensures a ****class only has one instance, and provides a global point of access to it****. The class keeps track of its sole instance and ensures that no other instance can be created. Singleton classes are appropriate for situations where it makes sense for a single object to provide access to a global resource. It usually uses lazy loading to create the single instance when it’s needed the first time.

#### - Adapter

Adapter converts class interface for other, expected by client. Adapter allows to objects to cooperate with objects they could not normally work with due to different interfaces.

#### - Repository

Repository pattern is a software design pattern that provides an abstraction of data, so that your application can work with a simple abstraction that has an interface.

It also makes code more ****testable**** as it allow us to inject as a dependency a mock repository that implements that defined interface.

## API ![](https://cdn.freebiesupply.com/logos/large/2x/deezer-logo-png-transparent.png =70x40)

The used API gives unlimited access, without stress, without identification.

Deezer Simple API provides a nice set of services to build up web applications allowing the discovery of Deezer's music catalogue.

[More about the API & Deezer](https://developers.deezer.com/)

![](http://pluspng.com/img-png/deezer-png-open-1000.png =160x40)

### Global parameters

Deezer API provides you some global parameters in order to simplify and organize your requests.

#### Pagination parameters

When the return of your request is a list of objects, you do not have to get the whole result in one time, you can paginate it if you want. Some parameters can be used to constrain the number of objects the request returns

#### Requests examples

##### https://api.deezer.com/playlist/4341978/tracks?index=0&limit=10

##### https://api.deezer.com/playlist/4341978/tracks?index=3&limit=7

##### https://api.deezer.com/playlist/4341978/tracks?limit=2

#### Request methods

Deezer API is a RESTful API, it means you have to use GET HTTP request in order to get informations, POST HTTP request to update/add datas and DELETE HTTP request to delete them.

A special parameter ****request_method**** is provided in order to override the HTTP request and allows to POST or DELETE request by using GET HTTP request instead.

## Examples  to follow

* ##### Use this format for convenience:
* ###### MARK:
// MARK: Something
func name(_ something:)

* ###### viewDidLoad():
// MARK: - Lifecycle Functions
override func viewDidLoad() {
super.viewDidLoad()
ActivityIndicatorManager.shared.createIndicator(imageView: self.activityIndicatorImageView, view: self.view)
requestChartArtists()
setUp()
}

* ###### Protocols:
func/protocol nameOfFunc/nameOfProtocol {
// Methods and variables
} // no empty line after that


## OTHER

- Programming Language - Swift

- [A way of commiting]([https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet))
