//
//  UINavigationController+Helpers.swift
//  music-app
//
//  Created by Stefan Aleksiev on 24.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

extension UINavigationController {
    func setNavBackground(navBarController: UINavigationController){
        navBarController.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBarController.navigationBar.shadowImage = UIImage()
        navBarController.navigationBar.isTranslucent = true
    }
}
