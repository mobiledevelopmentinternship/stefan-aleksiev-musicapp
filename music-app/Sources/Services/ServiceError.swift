//
//  Broth.swift
//  music-app
//
//  Created by Stefan Aleksiev on 23.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

enum ServiceError: Int, Error, CustomNSError {
    case fetchingChartData = 1
    case fetchingArtistData
    case fetchingAlbumData
    case fetchingTrackData
    case fetchingPlaylistData
    case unknownError
    
    static var errorDomain: String = "com.app.music.custom.error"
}

enum RequestError {
    static let errorTitle = "Unexpected Error"
    static let fetchingPlaylistError = "Problem with playlist data"
    static let fetchingArtistError = "Problem with artists data"
    static let fetchingAlbumError = "Problem with albums data"
    static let fetchingTrackError = "Problem with tracks data"
    static let fetchingChartError = "Problem with chart data"
    static let redirectionMessage = "There is no data for the current artist.\n You will be redirected to the previous screen."
}
