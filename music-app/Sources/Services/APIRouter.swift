//
//  APIRequest.swift
//  music-app
//
//  Created by Stefan Aleksiev on 9.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

enum APIRouter {
    
    case chartArtists
    case chartAlbums
    case chartPlaylists
    
    private var baseURL: String {
        return "https://api.deezer.com"
    }
    
    private var path: String {
        switch self {
        case .chartArtists:
            return "/chart/0/artists"
        case .chartAlbums:
            return "/chart/0/albums"
        case .chartPlaylists:
            return "/chart/0/playlists"
        }
    }
    
    private var method: HTTPMethod {
        switch self {
        case .chartArtists, .chartAlbums, .chartPlaylists:
            return .get
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        guard let url = URL(string: baseURL) else { throw AppError.wrongURL }
        
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.httpMethod = method.rawValue
        
        return request
    }
}

enum AppError: Error {
    case wrongURL
}

enum HTTPMethod: String {
    case get = "GET"
}

