//
//  CoreDataStack.swift
//  music-app
//
//  Created by Stefan Aleksiev on 12.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataStack: EntityTypeable {
    
    // MARK: - Singleton
    static let shared = CoreDataStack()
    
    func getEntityType(type: EntityType) -> String {
        switch type {
        case .playlist:
            return Constants.Entities.playlist
        case .artist:
            return Constants.Entities.artist
        case .album:
            return Constants.Entities.album
        case .track:
            return Constants.Entities.track
        }
    }
    
    func saveEntity(entityType: EntityType, model: Any, view: UIViewController) {
        let managedContext = self.persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: getEntityType(type: entityType), in: managedContext) else { return }
        let managedEntity = NSManagedObject(entity: entity, insertInto: managedContext)
        guard let entityName = managedEntity.entity.name else { return }
        
        switch entityName {
        case Constants.Entities.playlist:
            guard
                let playlistEntity = NSManagedObject(entity: entity, insertInto: managedContext) as? PlaylistEntity,
                let model = model as? Playlist else { return }
            
            guard isExist(entityType: EntityType(rawValue: EntityType.playlist.rawValue) ?? EntityType.playlist, id: model.id) else {
                addObject(entity: playlistEntity,id: model.id, title: model.title, picture: model.picture, type: model.type, tracklist: model.trackList)
                showAlert(title: Constants.saveMessageResponse.saveTitleResponse.success, message: Constants.saveMessageResponse.success, view: view)
                return
            }
            showAlert(title: Constants.saveMessageResponse.saveTitleResponse.failure, message: Constants.saveMessageResponse.failure, view: view)
            
        case Constants.Entities.artist:
            guard
                let artistEntity = NSManagedObject(entity: entity, insertInto: managedContext) as? ArtistEntity,
                let model = model as? Artist else { return }
            
            guard isExist(entityType: EntityType(rawValue: EntityType.artist.rawValue) ?? EntityType.artist, id: model.id) else {
                addObject(entity: artistEntity, id: model.id, title: model.title, picture: model.picture, type: model.type, tracklist: model.tracklist)
                showAlert(title: Constants.saveMessageResponse.saveTitleResponse.success, message: Constants.saveMessageResponse.success, view: view)
                return
            }
            showAlert(title: Constants.saveMessageResponse.saveTitleResponse.failure, message: Constants.saveMessageResponse.failure, view: view)
            
        case Constants.Entities.album:
            guard
                let albumEntity = NSManagedObject(entity: entity, insertInto: managedContext) as? AlbumEntity,
                let model = model as? Album else { return }
            
            guard isExist(entityType: EntityType(rawValue: EntityType.album.rawValue) ?? EntityType.album, id: model.id) else {
                addObject(entity: albumEntity,id: model.id, title: model.title, picture: model.picture, type: model.type, tracklist: model.tracklist)
                showAlert(title: Constants.saveMessageResponse.saveTitleResponse.success, message: Constants.saveMessageResponse.success, view: view)
                return
            }
            showAlert(title: Constants.saveMessageResponse.saveTitleResponse.failure, message: Constants.saveMessageResponse.failure, view: view)
            
        case Constants.Entities.track:
            guard
                let trackEntity = NSManagedObject(entity: entity, insertInto: managedContext) as? TrackEntity,
                let model = model as? Track else { return }
            
            guard isExist(entityType: EntityType(rawValue: EntityType.track.rawValue) ?? EntityType.track, id: model.id) else {
                addTrackObject(entity: trackEntity, id: model.id, type: model.type, title: model.title, preview: model.preview, duration: model.duration)
                showAlert(title: Constants.saveMessageResponse.saveTitleResponse.success, message: Constants.saveMessageResponse.success, view: view)
                return
            }
            showAlert(title: Constants.saveMessageResponse.saveTitleResponse.failure, message: Constants.saveMessageResponse.failure, view: view)
            
        default:
            break
        }
    }
    
    func addObject(entity: NSManagedObject, id: Int, title: String, picture: String, type: String, tracklist: String) {
        let managedContext = self.persistentContainer.viewContext
        entity.setValue(id, forKey: Constants.Entities.value.id)
        entity.setValue(title, forKey: Constants.Entities.value.title)
        entity.setValue(picture, forKey: Constants.Entities.value.picture)
        entity.setValue(type, forKey: Constants.Entities.value.type)
        entity.setValue(tracklist, forKey: Constants.Entities.value.tracklist)
        saveManagedContext(managedContext: managedContext)
    }
    
    func addTrackObject(entity: NSManagedObject, id: Int, type: String, title: String, preview: String, duration: Int) {
        let managedContext = self.persistentContainer.viewContext
        entity.setValue(id, forKey: Constants.Entities.value.id)
        entity.setValue(type, forKey: Constants.Entities.value.type)
        entity.setValue(title, forKey: Constants.Entities.value.title)
        entity.setValue(preview, forKey: Constants.Entities.value.preview)
        entity.setValue(duration, forKey: Constants.Entities.value.duration)
        saveManagedContext(managedContext: managedContext)
    }
    
    func saveManagedContext(managedContext: NSManagedObjectContext) {
        do {
            try managedContext.save()
        } catch let error as NSError {
            showErrorAlert(error: error)
        }
    }
    
    func fetchData<T:NSManagedObject>(entity: T.Type) -> [T] {
        let managedContext = self.persistentContainer.viewContext
        let entityName = String(describing: T.self)
        let request = NSFetchRequest<T>(entityName: entityName)
        
        guard let objects = try? managedContext.fetch(request) else { return [T]() }
        return objects
    }
    
    func deleteData(entityType: EntityType, id: Int) {
        let managedContext = self.persistentContainer.viewContext
        let entityName = getEntityType(type: entityType)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        let predicate = NSPredicate(format: "id == \(id)", id)
        fetchRequest.predicate = predicate
        
        do {
            let objects = try managedContext.fetch(fetchRequest)
            for object in objects {
                managedContext.delete(object)
            }
            try managedContext.save()
        } catch let error as NSError {
            showErrorAlert(error: error)
        }
    }
    
    func isExist(entityType: EntityType, id: Int) -> Bool {
        let managedContext = self.persistentContainer.viewContext
        let entity = getEntityType(type: entityType)
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entity)
        let filterPredicate = NSPredicate(format: "id == \(id)", id)
        fetchRequest.predicate = filterPredicate
        
        do {
            let count = try managedContext.count(for: fetchRequest)
            return count > 0 ? true : false
        } catch let error as NSError {
            showErrorAlert(error: error)
            return false
        }
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "music_app")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                self.showErrorAlert(error: error)
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError {
                showErrorAlert(error: error)
            }
        }
    }
    
    // MARK: - Additional Functions
    func showAlert(title: String, message:String, view: UIViewController) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: Constants.Alert.actionTitle, style: .default)
            alert.addAction(ok)
            view.present(alert, animated: true)
        }
    }
    
    func showErrorAlert(error: NSError) {
        guard let topVC = UIApplication.topViewController() else { return }
        showAlert(title: String(error.code), message: error.localizedDescription, view: topVC)
    }
}

enum EntityType: String {
    case playlist
    case artist
    case album
    case track
}

protocol EntityTypeable {
    func getEntityType(type: EntityType) -> String
}
