//
//  APIService.swift
//  music-app
//
//  Created by Stefan Aleksiev on 25.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import UIKit

typealias BaseCompletionHandler = ((Result<Data>) -> ())

class APIService: ServiceProtocol {

    func chartArtists(completionHandler: @escaping BaseCompletionHandler) {
        if let url = try? APIRouter.chartArtists.asURLRequest() {
            let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
                if let error = error {
                    completionHandler(.failure(error as NSError))
                    return
                }
                guard let data = data else { return }
                completionHandler(.success(data))
            }
            task.resume()
        }
    }
    
    func chartAlbums(completionHandler: @escaping BaseCompletionHandler) {
        if let url = try? APIRouter.chartAlbums.asURLRequest() {
            let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
                if let error = error {
                    completionHandler(.failure(error as NSError))
                    return
                }
                guard let data = data else { return }
                completionHandler(.success(data))
            }
            task.resume()
        }
    }
    
    func chartPlaylists(completionHandler: @escaping BaseCompletionHandler) {
        if let url = try? APIRouter.chartPlaylists.asURLRequest() {
            let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
                if let error = error {
                    completionHandler(.failure(error as NSError))
                    return
                }
                guard let data = data else { return }
                completionHandler(.success(data))
            }
            task.resume()
        }
    }
    
    func artistTracklist(completionHandler: @escaping BaseCompletionHandler) {
        if let url = AppManager.shared.albumTracklistURL {
            let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
                if let error = error {
                    completionHandler(.failure(error as NSError))
                    return
                }
                guard let data = data else { return }
                completionHandler(.success(data))
            }
            task.resume()
        }
    }
    
    func albumTracklist(completionHandler: @escaping BaseCompletionHandler) {
        if let url = AppManager.shared.albumTracklistURL {
            let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
                if let error = error {
                    completionHandler(.failure(error as NSError))
                    return
                }
                guard let data = data else { return }
                completionHandler(.success(data))
            }
            task.resume()
        }
    }
}

