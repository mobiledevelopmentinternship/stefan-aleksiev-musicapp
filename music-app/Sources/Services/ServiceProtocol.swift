//
//  ServiceProtocol.swift
//  music-app
//
//  Created by Stefan Aleksiev on 9.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

protocol ServiceProtocol {
    // TODO: -- Chart Requests --
    func chartArtists(completionHandler: @escaping (Result<Data>) -> ())
    func chartAlbums(completionHandler: @escaping (Result<Data>) -> ())
    func chartPlaylists(completionHandler: @escaping (Result<Data>) -> ())
    
    // TODO: -- Artist Requests --
    func artistTracklist(completionHandler: @escaping (Result<Data>) -> ())
    func albumTracklist(completionHandler: @escaping (Result<Data>) -> ())
}

enum Result<Value> {
    case success(Value)
    case failure(Error)
}
