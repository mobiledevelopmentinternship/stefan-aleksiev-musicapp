//
//  ImageWorker.swift
//  music-app
//
//  Created by Stefan Aleksiev on 22.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation
import UIKit

struct ImageWorker {
    private var url: String
    
    init(with url: String) {
        self.url = url
    }
    
    func loadImage() -> UIImage {
        var image = UIImage()
        guard let imageUrl = URL(string: url), let imageData = NSData(contentsOf: imageUrl) else {
            guard let defaultImage = UIImage(named: url) else { return image }
            return  defaultImage
        }
        image = UIImage(data: imageData as Data) ?? image
        return image
    }
}
