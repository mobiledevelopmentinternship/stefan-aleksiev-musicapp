//
//  ArtistTopAlbumsWorker.swift
//  music-app
//
//  Created by Stefan Aleksiev on 14.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct ArtistTopAlbumsWorker {
    
    private let service: ServiceProtocol
    
    init(service: ServiceProtocol) {
        self.service = service
    }
    
    func request(onSuccessCompletionHandler: @escaping OnSuccessCompletionHandlerAlbum,
                 onErrorHandler: @escaping OnErrorHandler)  {
        
        var albumsArray = [Album]()
        let emptyAlbum = Album()
        
        service.artistTracklist { result in
            switch result {
            case .success(let data):
                let jsonResponse = try? JSONSerialization.jsonObject(with:data, options: []) as? [String:Any]
                
                guard let albums = jsonResponse?["data"] as? [[String: Any]] else {
                    return
                }
                // TODO: -- Update logic by using map/flatmap instead of for loop --
                if !albums.isEmpty {
                    for index in 0...albums.count-1 {
                        guard let album = albums[index]["album"] as? [String: Any] else { return }
                        albumsArray.append(Album(album) ?? emptyAlbum)
                    }
                    onSuccessCompletionHandler(albumsArray)
                } else {
                    onSuccessCompletionHandler(albumsArray)
                }
            case .failure(_):
                let error = NSError(domain:ServiceError.errorDomain, code:ServiceError.fetchingAlbumData.rawValue, userInfo:[ NSLocalizedDescriptionKey: RequestError.fetchingAlbumError])
                onErrorHandler(error)
            }
        }
    }
}
