
//
//  File.swift
//  music-app
//
//  Created by Stefan Aleksiev on 13.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

typealias OnSuccessCompletionHandlerTrack = ([Track]) -> ()

struct ArtistTopTracksWorker {
    
    private let service: ServiceProtocol
    
    init(service: ServiceProtocol) {
        self.service = service
    }
    
    func request(onSuccessCompletionHandler: @escaping OnSuccessCompletionHandlerTrack,
                 onErrorHandler: @escaping OnErrorHandler)  {
        
        var tracksArray = [Track]()
        let emptyTrack = Track()
        
        service.artistTracklist { result in
            switch result {
            case .success(let data):
                let jsonResponse = try? JSONSerialization.jsonObject(with:data, options: []) as? [String:Any]
                
                if let tracks = jsonResponse?["data"] as? [[String: Any]] {
                    tracksArray = tracks.map{Track($0) ?? emptyTrack}
                    onSuccessCompletionHandler(tracksArray)
                } else {
                    onSuccessCompletionHandler(tracksArray)
                }
            case .failure(_):
                let error = NSError(domain:ServiceError.errorDomain, code:ServiceError.fetchingTrackData.rawValue, userInfo:[ NSLocalizedDescriptionKey: RequestError.fetchingTrackError])
                onErrorHandler(error)
            }
        }
    }
}
