//
//  ChartArtistWorker.swift
//  music-app
//
//  Created by Stefan Aleksiev on 9.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

typealias OnSuccessCompletionHandlerArtist = ([Artist]) -> ()
typealias OnErrorHandler = (NSError) -> ()

struct ChartArtistWorker {
    
    private let service: ServiceProtocol
    
    init(service: ServiceProtocol) {
        self.service = service
    }
    
    func request(onSuccessCompletionHandler: @escaping OnSuccessCompletionHandlerArtist,
                 onErrorHandler: @escaping OnErrorHandler)  {
        
        var artistsArray = [Artist]()
        let emptyArtist = Artist()
        
        service.chartArtists { result in
            switch result {
            case .success(let data):
                let jsonResponse = try? JSONSerialization.jsonObject(with:data, options: []) as? [String:Any]
                
                if let artists = jsonResponse?["data"] as? [[String: Any]] {
                    artistsArray = artists.map{Artist($0) ?? emptyArtist}
                    onSuccessCompletionHandler(artistsArray)
                    
                } else {
                    onSuccessCompletionHandler(artistsArray)
                }
            case .failure(_):
                let error = NSError(domain:ServiceError.errorDomain, code:ServiceError.fetchingChartData.rawValue, userInfo:[ NSLocalizedDescriptionKey: RequestError.fetchingChartError])
                onErrorHandler(error)
            }
        }
    }
}
