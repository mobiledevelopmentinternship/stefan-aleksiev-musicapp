//
//  ChartAlbumWorker.swift
//  music-app
//
//  Created by Stefan Aleksiev on 10.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

typealias OnSuccessCompletionHandlerAlbum = ([Album]) -> ()

struct ChartAlbumWorker {
    
    private let service: ServiceProtocol
    
    init(service: ServiceProtocol){
        self.service = service
    }
    
    func request(onSuccessCompletionHandler: @escaping OnSuccessCompletionHandlerAlbum,
                 onErrorHandler: @escaping OnErrorHandler)  {
        
        var albumsArray = [Album]()
        let emptyAlbum = Album()
        
        service.chartAlbums { result in
            switch result {
            case .success(let data):
                
                let jsonResponse = try? JSONSerialization.jsonObject(with:data, options: []) as? [String:Any]
                
                if let albums = jsonResponse?["data"] as? [[String: Any]] {
                    albumsArray = albums.map{Album($0) ?? emptyAlbum}
                    onSuccessCompletionHandler(albumsArray)
                } else {
                    onSuccessCompletionHandler(albumsArray)
                }
            case .failure(_):
                let error = NSError(domain:ServiceError.errorDomain, code:ServiceError.fetchingAlbumData.rawValue, userInfo:[ NSLocalizedDescriptionKey: RequestError.fetchingAlbumError])
                onErrorHandler(error)
            }
        }
    }
}
