//
//  ChartPlaylistWorker.swift
//  music-app
//
//  Created by Stefan Aleksiev on 10.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

typealias OnSuccessCompletionHandlerPlaylist = (Playlist) -> ()

struct ChartPlaylistWorker {
    
    private let service: ServiceProtocol
    
    init(service: ServiceProtocol){
        self.service = service
    }
    
    func request(onSuccessCompletionHandler: @escaping OnSuccessCompletionHandlerPlaylist,
                 onErrorHandler: @escaping OnErrorHandler)  {
        
        service.chartPlaylists { result in
            switch result {
            case .success(let data):
                var playlist: Playlist!
                let jsonResponse = try? JSONSerialization.jsonObject(with:data, options: []) as? [String:Any]
                
                if let playlists = jsonResponse?["data"] as? [[String: Any]] {
                    playlist = (Playlist(playlists[0]))
                    onSuccessCompletionHandler(playlist)
                } else {
                    onSuccessCompletionHandler(playlist)
                }
            case .failure(_):
                let error = NSError(domain:ServiceError.errorDomain, code:ServiceError.fetchingPlaylistData.rawValue, userInfo:[ NSLocalizedDescriptionKey: RequestError.fetchingPlaylistError])
                onErrorHandler(error)
            }
        }
    }
}
