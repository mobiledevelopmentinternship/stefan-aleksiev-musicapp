//
//  AppManager.swift
//  music-app
//
//  Created by Stefan Aleksiev on 13.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class AppManager {
    
    // MARK: - Singleton 
    static let shared = AppManager()
    
    public let dispatchGroup = DispatchGroup()
    public private(set) var albumTracklistURL: URL?
    public private(set) var playlistTracklistURL: URL?
    public private(set) var recommendedArtists = [Artist]()
    
    private init(){}
    
    func setAlbumTracklist(url: URL?) {
        self.albumTracklistURL = url
    }
    
    func setPlaylistTracklist(url: URL?) {
        self.playlistTracklistURL = url
    }
    
    func setRecommendedArtists(artists: [Artist]) {
        self.recommendedArtists = artists
    }
    
    func redirectBack(image: UIImageView,viewController: UIViewController) {
        DispatchQueue.main.async {
            ActivityIndicatorManager.shared.stopIndicator(imageView: image)
            ActivityIndicatorManager.shared.removeTransparentView(view: viewController.view)
        }
        viewController.showEmptyDataAlert(title: RequestError.errorTitle, message: RequestError.redirectionMessage)
    }
}
