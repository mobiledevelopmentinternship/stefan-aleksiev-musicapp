//
//  NetworkManager.swift
//  music-app
//
//  Created by Stefan Aleksiev on 27.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

class NetworkManager: NSObject {
    var reachability: Reachability!
    
    static let shared: NetworkManager = {
        return NetworkManager()
    }()
    
    override init() {
        super.init()
        reachability = Reachability()
        
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanges(_:)), name: .reachabilityChanged, object: reachability)
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func networkStatusChanges(_ notification: Notification) {}
    
    static func stopNotifier() {
        do {
            try (NetworkManager.shared.reachability).startNotifier()
        } catch {
            print("Error stopping the notifier")
        }
    }
    
    // Network is reachable
    static func isReachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.shared.reachability).connection != .none {
            completed(NetworkManager.shared)
        }
    }
    
    // Network is unreachable
    static func isUnreachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.shared.reachability).connection == .none {
            completed(NetworkManager.shared)
        }
    }
    
    // Network is reachable via WWAN / Cellular
    static func isReachableViaWWAN(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.shared.reachability).connection == .cellular {
            completed(NetworkManager.shared)
        }
    }
    
    // Network is reachable via WiFi
    static func isReachableViaWiFi(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.shared.reachability).connection == .wifi {
            completed(NetworkManager.shared)
        }
    }
}

