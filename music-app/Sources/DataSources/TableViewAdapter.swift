//
//  TableViewAdapter.swift
//  music-app
//
//  Created by Stefan Aleksiev on 24.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class TableViewAdapter: NSObject, UITableViewDataSource, UITableViewDelegate, ObjectTypeable {
    
    // MARK: - Properties
    private var tracks = [Track]()
    private var artists = [Artist]()
    private var albums = [Album]()
    private var playlists = [Playlist]()
    private var album: Album!
    private var artist: Artist!
    private var playlist: Playlist!
    private var track: Track!
    private var type = ""
    private var itemsInSection = 0
    private var numberOfSections = 0
    private var sectionNames = [String]()
    private let sectionImages = [
        Constants.ImagePath.SectionImages.playlist,
        Constants.ImagePath.SectionImages.artist,
        Constants.ImagePath.SectionImages.album,
        Constants.ImagePath.SectionImages.track
    ]
    private var isEditable = false
    weak var delegate: TableViewDataSourceAdapterDelegate?
    
    func setItems(numberOfItemsInSection: Int){
        itemsInSection = numberOfItemsInSection
    }
    
    func makeCellEditable() {
        isEditable = true
    }
    
    func updatePlaylistArray(with array: [Playlist]) {
        playlists = array
    }
    
    func updateTracksArray(with array: [Track]) {
        tracks = array
    }
    
    func updateArtistsArray(with array: [Artist]) {
        artists = array
    }
    
    func updateAlbumsArray(with array: [Album]) {
        albums = array
    }
    
    func updateAlbum(with album: Album) {
        type = album.type
        self.album = album
    }
    
    func updateArtist(with artist: Artist) {
        type = artist.type
        self.artist = artist
    }
    
    func updatePlaylist(with playlist: Playlist) {
        type = playlist.type
        self.playlist = playlist
    }
    
    func updateTrack(with track: Track) {
        type = track.type
        self.track = track
    }
    
    func setSectionViewHeader(with sectionName: String) {
        sectionNames.append(sectionName)
    }
    
    func getObjectType(type: ObjectType) -> String {
        switch type {
        case .album:
            return self.album.picture
        case .artist:
            return self.artist.picture
        case .playlist:
            return self.playlist.picture
        case .track:
            return Constants.ImagePath.defaultTrack
        }
    }
    
    func deleteCell(indexPath: IndexPath, id: Int, entityType: String, type: String) {
        CoreDataStack.shared.deleteData(entityType: EntityType(rawValue: entityType) ?? EntityType.artist, id: id)
        
        switch type {
        case ObjectType.artist.rawValue:
            self.artists.remove(at: indexPath.row)
        case ObjectType.album.rawValue:
            self.albums.remove(at: indexPath.row)
        case ObjectType.track.rawValue:
            self.tracks.remove(at: indexPath.row)
        case ObjectType.playlist.rawValue:
            self.playlists.remove(at: indexPath.row)
        default:
            break
        }
    }
    
    // MARK: - TableView Data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionNames.count == 0 ? 1 : self.sectionNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sectionNames[section] {
        case Constants.SectionHeaderTitles.tracks:
            return tracks.count
        case Constants.SectionHeaderTitles.albums:
            return albums.count
        case Constants.SectionHeaderTitles.artists:
            return artists.count
        case Constants.SectionHeaderTitles.playlist:
            return playlists.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.tableView(tableView, didSelectRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard isEditable else {
            return UISwipeActionsConfiguration()
        }
        let deleteAction = UIContextualAction(style: .normal, title: nil) { [weak self] action, index, complete  in
            guard let self = self else { return }
            switch self.sectionNames[indexPath.section] {
            case Constants.SectionHeaderTitles.artists:
                self.deleteCell(indexPath: indexPath, id: self.artists[indexPath.row].id, entityType: EntityType.artist.rawValue, type: ObjectType.artist.rawValue)
            case Constants.SectionHeaderTitles.albums:
                self.deleteCell(indexPath: indexPath, id: self.albums[indexPath.row].id, entityType: EntityType.album.rawValue, type: ObjectType.album.rawValue)
            case Constants.SectionHeaderTitles.tracks:
                self.deleteCell(indexPath: indexPath, id: self.tracks[indexPath.row].id, entityType: EntityType.track.rawValue, type: ObjectType.track.rawValue)
            case Constants.SectionHeaderTitles.playlist:
                self.deleteCell(indexPath: indexPath, id: self.playlists[indexPath.row].id, entityType: EntityType.playlist.rawValue, type: ObjectType.playlist.rawValue)
            default:
                break
            }
            tableView.deleteRows(at: [indexPath], with: .automatic)
            complete(true)
        }
        deleteAction.image = UIImage(named: Constants.ImagePath.trashIcon)
        deleteAction.backgroundColor = UIColor.white.withAlphaComponent(0)
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.sectionNames.count > 0 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SectionHeaderView.reuseIdentifier) as! SectionHeaderView
            headerView.configure(with: sectionNames[section], with: sectionImages[section])
            return headerView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sectionNames[indexPath.section] {
        case Constants.SectionHeaderTitles.artists:
            guard let artistCell = tableView.dequeueReusableCell(withIdentifier: ArtistTableViewCell.reuseIdentifier, for: indexPath) as? ArtistTableViewCell else { return UITableViewCell() }
            updateArtist(with: artists[indexPath.row])
            artistCell.configure(with: getObjectType(type: ObjectType(rawValue: type) ?? ObjectType.artist), with: artists[indexPath.row].title)
            return artistCell
        case Constants.SectionHeaderTitles.albums:
            guard let albumCell = tableView.dequeueReusableCell(withIdentifier: AlbumTableViewCell.reuseIdentifier, for: indexPath) as? AlbumTableViewCell else { return UITableViewCell() }
            updateAlbum(with: albums[indexPath.row])
            albumCell.configure(with: getObjectType(type: ObjectType(rawValue: type) ?? ObjectType.album), with: albums[indexPath.row].title)
            return albumCell
        case Constants.SectionHeaderTitles.tracks:
            guard let trackCell = tableView.dequeueReusableCell(withIdentifier: TrackTableViewCell.reuseIdentifier, for: indexPath) as? TrackTableViewCell else { return UITableViewCell() }
            updateTrack(with: tracks[indexPath.row])
            trackCell.configure(with: getObjectType(type: ObjectType(rawValue: type) ?? ObjectType.track), with: tracks[indexPath.row].title)
            return trackCell
        case Constants.SectionHeaderTitles.playlist:
            guard let playlistCell = tableView.dequeueReusableCell(withIdentifier: AlbumTableViewCell.reuseIdentifier, for: indexPath) as? AlbumTableViewCell else { return UITableViewCell() }
            updatePlaylist(with: playlists[indexPath.row])
            playlistCell.configure(with: getObjectType(type: ObjectType(rawValue: type) ?? ObjectType.playlist), with: playlists[indexPath.row].title)
            return playlistCell
        default:
            break
        }
        return UITableViewCell()
    }
}

protocol TableViewDataSourceAdapterDelegate: class {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
}

