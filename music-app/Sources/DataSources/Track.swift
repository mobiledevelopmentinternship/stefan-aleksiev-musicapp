//
//  Track.swift
//  music-app
//
//  Created by Stefan Aleksiev on 7.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct Track: ModelTypeable {
    
    let id: Int
    var type: String
    let title: String
    let preview: String
    let duration: Int
    
    init?(_ dictionary: [String: Any]) {
        guard let id = dictionary["id"] as? Int , let type = dictionary["type"] as? String else {
            return nil
        }
        self.id = id
        self.type = type
        title = dictionary["title"] as? String ?? ""
        preview = dictionary["preview"] as? String ?? ""
        duration = dictionary["duration"] as? Int ?? 0
    }
    
    init(id: Int, type: String, title: String, preview: String, duration: Int) {
        self.id = id
        self.type = type
        self.title = title
        self.preview = preview
        self.duration = duration
    }
    
    init() {
        self.id = 0
        self.type = "track"
        self.title = "title"
        self.preview = "preview"
        self.duration = 0
    }
}
