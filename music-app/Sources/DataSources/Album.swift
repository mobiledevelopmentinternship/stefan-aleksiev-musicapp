//
//  Album.swift
//  music-app
//
//  Created by Stefan Aleksiev on 7.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct Album: ModelTypeable {
    
    let id: Int
    var type: String
    private(set) var title: String
    private(set) var picture: String
    private(set) var tracklist: String
    
    init?(_ dictionary: [String: Any]) {
        guard let id = dictionary["id"] as? Int , let type = dictionary["type"] as? String else {
            return nil
        }
        self.id = id
        self.type = type
        title = dictionary["title"] as? String ?? ""
        picture = dictionary["cover"] as? String ?? ""
        tracklist = dictionary["tracklist"] as? String ?? ""
    }
    
    init(id: Int, type: String, title: String, picture: String, tracklist: String) {
        self.id = id
        self.type = type
        self.title = title
        self.picture = picture
        self.tracklist = tracklist
    }
    
    init() {
        self.id = 1
        self.type = "album"
        self.title = "title"
        self.picture = "picture"
        self.tracklist = "tracklist"
    }
}
