//
//  Playlist.swift
//  music-app
//
//  Created by Stefan Aleksiev on 7.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct Playlist: ModelTypeable {
    
    let id: Int
    var type: String
    private(set) var title: String
    private(set) var picture: String
    private(set) var trackList: String
    
    init?(_ dictionary: [String: Any]) {
        guard let id = dictionary["id"] as? Int , let type = dictionary["type"] as? String else {
            return nil
        }
        self.id = id
        self.type = type
        title = dictionary["title"] as? String ?? ""
        picture = dictionary["picture_xl"] as? String ?? ""
        trackList = dictionary["tracklist"] as? String ?? ""
    }
    
    init(_ playlist: Playlist) {
        id = playlist.id
        type = playlist.type
        title = playlist.title
        picture = playlist.picture
        trackList = playlist.trackList
    }
    
    init(id: Int, type: String, title: String, picture: String, tracklist: String) {
        self.id = id
        self.type = type
        self.title = title
        self.picture = picture
        self.trackList = tracklist
    }
    
    
}
