//
//  ArtistsDataSource.swift
//  music-app
//
//  Created by Stefan Aleksiev on 22.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class CollectionViewAdapter: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    private var artists = [Artist]()
    private var recommendedArtists = [Artist]()
    private var artistPhoto : Artist!
    private var albums = [Album]()
    private var tracks = [Track]()
    weak var delegate: CollectionViewDataSourceAdapterDelegate?
    private var itemsInSection = 0
    private var reuseIdentifier = ""
    
    init(cellReuseIdentifier: String) {
        self.reuseIdentifier = cellReuseIdentifier
    }
    
    func setItems(numberOfItemsInSection: Int){
        self.itemsInSection = numberOfItemsInSection
    }
    
    func updateArtistArray(with array: [Artist]) {
        self.artists = array
    }
    
    func updateAlbumsArray(with array: [Album]) {
        self.albums = array
    }
    
    func updateTrackArray(with array: [Track]) {
        self.tracks = array
    }
    
    func updateTrackImage(with artistPhoto: Artist) {
        self.artistPhoto = artistPhoto
    }
    
    func updateTracksArray(with array: [Track]) {
        self.tracks = array
    }
    
    func updateRecommendedArtists(with array: [Artist]) {
        self.recommendedArtists = array
    }
    
    // MARK: UICollectionsViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsInSection
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.collectionView(collectionView, didSeselectItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let defaultCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        switch reuseIdentifier {
        case ArtistCollectionViewCell.reuseIdentifier:
            guard let artistCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? ArtistCollectionViewCell else { return  defaultCell }
            artistCell.configure(with: artists[indexPath.row].picture)
            return artistCell
        case AlbumCollectionViewCell.reuseIdentifier:
            guard let albumCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? AlbumCollectionViewCell else { return  defaultCell }
            albumCell.configure(with: albums[indexPath.row].picture)
            return albumCell
        case TrackCollectionViewCell.reuseIdentifier:
            guard let trackCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? TrackCollectionViewCell else { return  defaultCell }
            trackCell.configure(with: artistPhoto.picture, with: tracks[indexPath.row].title)
            return trackCell
        case RecommendedCollectionViewCell.reuseIdentifier:
            guard let recommendedCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? RecommendedCollectionViewCell else { return  defaultCell }
            recommendedCell.configure(with: recommendedArtists[indexPath.row].picture)
            return recommendedCell
        default:
            break
        }
        return defaultCell
    }
}

protocol CollectionViewDataSourceAdapterDelegate: class {
    func collectionView(_ collectionView: UICollectionView, didSeselectItemAt indexPath: IndexPath)
}

