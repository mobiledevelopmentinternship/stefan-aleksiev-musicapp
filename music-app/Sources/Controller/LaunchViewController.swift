//
//  LaunchViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 27.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    let network = NetworkManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            NetworkManager.isUnreachable { [weak self] _ in
                self?.showOfflinePage()
            }
            
            NetworkManager.isReachable { [weak self] _ in
                self?.showOnlinePage()
            }
        }
    }
    
    private func showOfflinePage() {
        DispatchQueue.main.async {
            self.performSegue(
                withIdentifier: "NetworkUnavailable",
                sender: self
            )
        }
    }
    
    private func showOnlinePage() {
        DispatchQueue.main.async {
            self.performSegue(
                withIdentifier: "NetworkAvailable",
                sender: self
            )
        }
    }
}
