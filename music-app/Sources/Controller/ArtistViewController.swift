//
//  ArtistsViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 24.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class ArtistViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicatorImageView: UIImageView!
    
    // MARK: - Properties
    private var artistDataSource: TableViewAdapter!
    private var recommendedArtists = [Artist]()
    private var artists = [Artist]()
    private var filteredArtists = [Artist]()
    private let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - Workers
    private var chartArtistsWorker = ChartArtistWorker(service: APIService())
    
    init(chartArtistsWorker: ChartArtistWorker) {
        self.chartArtistsWorker = chartArtistsWorker
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        requestChartArtists()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton(color: .white)
    }
}

// MARK: - Requests
private extension ArtistViewController {
    func requestChartArtists() {
        AppManager.shared.dispatchGroup.enter()
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView, view: self.view)
        chartArtistsWorker.request(onSuccessCompletionHandler: { [weak self] artists in
            guard let self = self else { return }
            if artists.isEmpty {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.artists = artists
            }
            self.reloadData()
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func reloadData() {
        AppManager.shared.dispatchGroup.notify(queue: .main) {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
            
            self.setDataSource()
            self.tableView.reloadData()
        }
    }
    
    func stopIndicator() {
        DispatchQueue.main.async {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
        }
    }
}

// MARK: - Internal Logic
private extension ArtistViewController {
    func setUp(){
        recommendedArtists = artists
        activityIndicatorImageView.center = self.view.center
        setUpTableView()
        setNavigationItems()
        searchBarSetup()
    }
    
    func searchBarSetup() {
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.placeholder = Constants.SearchBar.artistPlaceholder
        searchController.searchBar.barStyle = .default
        searchController.searchBar.setBackground()
        searchController.searchBar.subviews[0].subviews.compactMap(){ $0 as? UITextField }.first?.tintColor = .gray
        
        navigationItem.searchController = searchController
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController?.searchBar.tintColor = .white
    }
    
    func setUpTableView() {
        tableView.register(UINib(nibName: ArtistTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ArtistTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: SectionHeaderView.reuseIdentifier, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: SectionHeaderView.reuseIdentifier)
        tableView.separatorStyle = .none
        
        artistDataSource = TableViewAdapter()
        artistDataSource.delegate = self
        artistDataSource.setSectionViewHeader(with: Constants.SectionHeaderTitles.artists)
        
        tableView.dataSource = artistDataSource
        tableView.delegate = artistDataSource
    }
    
    func setDataSource() {
        artistDataSource.setItems(numberOfItemsInSection: self.artists.count)
        artistDataSource.updateArtistsArray(with: self.artists)
    }
    
    func setNavigationItems() {
        navigationItem.rightBarButtonItems = [
            super.favouriteNavigationItem]
    }
}

extension ArtistViewController {
    func updateArtists(artists: [Artist]) {
        self.artists = artists
    }
}

extension ArtistViewController: TableViewDataSourceAdapterDelegate {
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = URL(string: artists[indexPath.row].tracklist) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingArtistError)
            return
        }
        let artistDetailsVC: ArtistDetailsViewController = self.instantiateViewController(with: ArtistDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        artistDetailsVC.updateArtist(artist: self.artists[indexPath.row])
        self.navigationController?.pushViewController(artistDetailsVC, animated: true)
    }
}

extension ArtistViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView,view: self.view)
        if searchText.isEmpty {
            artistDataSource.updateArtistsArray(with: artists)
            artistDataSource.setItems(numberOfItemsInSection: artists.count)
        } else {
            filteredArtists = artists
            self.filteredArtists = self.artists.filter({ artist -> Bool in
                guard let text = searchBar.text else { return false }
                return artist.title.lowercased().contains(text.lowercased())
            })
            artistDataSource.updateArtistsArray(with: filteredArtists)
            artistDataSource.setItems(numberOfItemsInSection: filteredArtists.count)
        }
        tableView.reloadData()
        stopIndicator()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView,view: self.view)
        guard let searchText = searchBar.text else { return }
        if !searchText.isEmpty {
            artistDataSource.updateArtistsArray(with: artists)
            artistDataSource.setItems(numberOfItemsInSection: artists.count)
            self.tableView.reloadData()
        }
        stopIndicator()
    }
}
