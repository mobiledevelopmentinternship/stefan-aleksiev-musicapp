//
//  FavouritesViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 4.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class FavouritesViewController: BaseViewController, ControllerTitle {
    
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var noDataYetLabel: UILabel!
    
    // MARK: - Properties
    private let searchController = UISearchController(searchResultsController: nil)
    private var favouritesDataSource: TableViewAdapter!
    private var sectionNames = [String]()
    private var tracksArray = [Track]()
    private var albumsArray = [Album]()
    private var artistsArray = [Artist]()
    private var playlistsArray = [Playlist]()
    
    var navigationTitle: String {
        return Constants.NavigationTitles.favouritesTitle
    }
    
    // MARK: - Workers
    private var coreDataStack = CoreDataStack()
    
    init(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackground()
        setBackButton(color: .white)
        searchBarSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
    }
}

// MARK: - Prepare and fetch data
extension FavouritesViewController: DataTypeable {
    private func setup() {
        setBackButton(color: .white)
        title = self.navigationTitle
        noDataYetLabel.center = self.view.center
        tableView.separatorStyle = .none
        
        let playlists = coreDataStack.fetchData(entity: PlaylistEntity.self)
        let artists = coreDataStack.fetchData(entity: ArtistEntity.self)
        let albums = coreDataStack.fetchData(entity: AlbumEntity.self)
        let tracks = coreDataStack.fetchData(entity: TrackEntity.self)
        
        mapTrack(entity: tracks)
        mapAlbum(entity: albums)
        mapArtist(entity: artists)
        mapPlaylist(entity: playlists)

        guard albumsArray.isEmpty && artistsArray.isEmpty && playlistsArray.isEmpty && tracksArray.isEmpty else {
            setUpTableView()
            return
        }
        noDataYetLabel.text = Constants.Labels.noDataInFavourites
    }
    
    private func setUpTableView() {
        tableView.register(UINib(nibName: ArtistTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ArtistTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: AlbumTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: AlbumTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: TrackTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: TrackTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: SectionHeaderView.reuseIdentifier, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: SectionHeaderView.reuseIdentifier)
        
        favouritesDataSource = TableViewAdapter()
        favouritesDataSource.delegate = self
        favouritesDataSource.makeCellEditable()
        
        tableView.dataSource = favouritesDataSource
        tableView.delegate = favouritesDataSource
        
        guard
            let trackValue = ObjectType(rawValue: ObjectType.track.rawValue),
            let albumValue = ObjectType(rawValue: ObjectType.album.rawValue),
            let artistValue = ObjectType(rawValue: ObjectType.artist.rawValue),
            let playlistValue = ObjectType(rawValue: ObjectType.playlist.rawValue)
        else { return }
        
        if !playlistsArray.isEmpty { setDataSource(type: playlistValue)}
        if !artistsArray.isEmpty { setDataSource(type: artistValue)}
        if !albumsArray.isEmpty { setDataSource(type: albumValue)}
        if !tracksArray.isEmpty { setDataSource(type: trackValue)}
    }
    
    func setDataSource(type: ObjectType) {
        switch type {
        case .album:
            sectionNames.append(Constants.SectionHeaderTitles.albums)
            favouritesDataSource.setSectionViewHeader(with: Constants.SectionHeaderTitles.albums)
            favouritesDataSource.updateAlbumsArray(with: albumsArray)
        case .artist:
            sectionNames.append(Constants.SectionHeaderTitles.artists)
            favouritesDataSource.setSectionViewHeader(with: Constants.SectionHeaderTitles.artists)
            favouritesDataSource.updateArtistsArray(with: artistsArray)
        case .playlist:
            sectionNames.append(Constants.SectionHeaderTitles.playlist)
            favouritesDataSource.setSectionViewHeader(with: Constants.SectionHeaderTitles.playlist)
            favouritesDataSource.updatePlaylistArray(with: playlistsArray)
        case .track:
            sectionNames.append(Constants.SectionHeaderTitles.tracks)
            favouritesDataSource.setSectionViewHeader(with: Constants.SectionHeaderTitles.tracks)
            favouritesDataSource.updateTracksArray(with: tracksArray)
        }
    }
    
    private func searchBarSetup() {
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.placeholder = Constants.SearchBar.favouritesPlaceholder
        searchController.searchBar.barStyle = .default
        searchController.searchBar.setBackground()
        searchController.searchBar.subviews[0].subviews.compactMap(){ $0 as? UITextField }.first?.tintColor = .gray
        
        navigationItem.searchController = searchController
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController?.searchBar.tintColor = .white
    }
    
    private func mapTrack(entity: [TrackEntity]) {
        tracksArray = entity.compactMap({
            guard let type = $0.type, let title = $0.title, let preview = $0.preview else { return nil }
            return Track(id: Int($0.id), type: type, title: title, preview: preview, duration: Int($0.duration))
        })
    }
    
    private func mapAlbum(entity: [AlbumEntity]) {
        albumsArray = entity.compactMap {
            guard let type = $0.type, let title = $0.title, let picture = $0.picture, let tracklist = $0.tracklist else { return nil }
            return Album(id: Int($0.id), type: type, title: title, picture: picture, tracklist: tracklist)
        }
    }
 
    private func mapArtist(entity: [ArtistEntity]) {
        artistsArray = entity.compactMap {
            guard let type = $0.type, let title = $0.title, let picture = $0.picture, let tracklist = $0.tracklist else { return nil }
            return Artist(id: Int($0.id), type: type, title: title, picture: picture, tracklist: tracklist)
        }
    }
    
    private func mapPlaylist(entity: [PlaylistEntity]) {
        playlistsArray = entity.compactMap {
            guard let type = $0.type, let title = $0.title, let picture = $0.picture, let tracklist = $0.tracklist else { return nil }
            return Playlist(id: Int($0.id), type: type, title: title, picture: picture, tracklist: tracklist)
        }
    }
}

extension FavouritesViewController: TableViewDataSourceAdapterDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch sectionNames[indexPath.section] {
        case indexSections.Albums.rawValue:
            navigateToAlbum(indexPath: indexPath)
        case indexSections.Artists.rawValue:
            navigateToArtist(indexPath: indexPath)
        case indexSections.Playlist.rawValue:
            navigateToPlaylist(indexPath: indexPath)
        case indexSections.Tracks.rawValue:
            presentMediaPlayer(indexPath: indexPath)
        default:
            self.showAlert(title: RequestError.errorTitle, message: Constants.Labels.typeError)
        }
    }
    
    private func navigateToAlbum(indexPath: IndexPath) {
        guard let url = URL(string: albumsArray[indexPath.row].tracklist) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingAlbumError)
            return
        }
        let albumDetailsVC: AlbumDetailsViewController = self.instantiateViewController(with: AlbumDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        albumDetailsVC.updateAlbum(self.albumsArray[indexPath.row])
        self.navigationController?.pushViewController(albumDetailsVC, animated: true)
    }
    
    private func navigateToArtist(indexPath: IndexPath) {
        guard let url = URL(string: artistsArray[indexPath.row].tracklist) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingArtistError)
            return
        }
        let artistDetailsVC: ArtistDetailsViewController = self.instantiateViewController(with: ArtistDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        artistDetailsVC.updateArtist(artist: self.artistsArray[indexPath.row])
        self.navigationController?.pushViewController(artistDetailsVC, animated: true)
    }
    
    private func navigateToPlaylist(indexPath: IndexPath) {
        guard let url = URL(string: playlistsArray[indexPath.row].trackList) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingPlaylistError)
            return
        }
        let albumDetailsVC: AlbumDetailsViewController = self.instantiateViewController(with: AlbumDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        albumDetailsVC.updatePlaylist(playlist: self.playlistsArray[indexPath.row])
        self.navigationController?.pushViewController(albumDetailsVC, animated: true)
    }
    
    private func presentMediaPlayer(indexPath: IndexPath) {
        self.view.addSubview(ActivityIndicatorManager.shared.transparentView)
        let mediaPlayer: MediaPlayerViewController = self.instantiateViewController(with: MediaPlayerViewController.reuseIdentifier)
        mediaPlayer.updateMedia(index: indexPath.row, tracks: tracksArray, imageUrl: Constants.ImagePath.defaultTrack, isFavouriteEnabled: false)
        self.present(mediaPlayer, animated: true)
    }
}

extension FavouritesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            setup()
            tableView.reloadData()
            return
        }
        artistsArray = artistsArray.filter({ items -> Bool in
            guard let text = searchBar.text else { return false }
            return items.title.lowercased().contains(text.lowercased())
        })
        
        albumsArray = albumsArray.filter({ items -> Bool in
            guard let text = searchBar.text else { return false }
            return items.title.lowercased().contains(text.lowercased())
        })
        
        playlistsArray = playlistsArray.filter({ items -> Bool in
            guard let text = searchBar.text else { return false }
            return items.title.lowercased().contains(text.lowercased())
        })
    
        tracksArray = tracksArray.filter({ items -> Bool in
            guard let text = searchBar.text else { return false }
            return items.title.lowercased().contains(text.lowercased())
        })
        
        setUpTableView()
        tableView.reloadData()
        return
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        setup()
        tableView.reloadData()
    }
}

enum indexSections: String {
    case Albums, Artists, Playlist, Tracks
}
