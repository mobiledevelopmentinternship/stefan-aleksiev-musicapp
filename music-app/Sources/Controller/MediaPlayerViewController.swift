//
//  MediaPlayerViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 24.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit
import AVFoundation

class MediaPlayerViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var trackImage: UIImageView!
    @IBOutlet private weak var trackLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var playBtn: UIButton!
    @IBOutlet private weak var nextBtn: UIButton!
    @IBOutlet private weak var previousBtn: UIButton!
    @IBOutlet private weak var favouritesBtn: UIButton!
    
    // MARK: - Properties
    private var track: Track!
    private var tracks = [Track]()
    private var imageUrl = ""
    private var player: AVPlayer?
    private var isPlaying = false
    private var isFavouriteEnabled = false
    private var index = 0
    
    // MARK: - Workers
    private var coreDataStack = CoreDataStack()
    
    init(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
}

// MARK: - Actions
private extension MediaPlayerViewController {
    @IBAction func closeBtn(_ sender: Any) {
        ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
        dismiss(animated: true)
    }
    
    @IBAction func addToFavouritesBtn(_ sender: Any) {
        coreDataStack.saveEntity(entityType: EntityType(rawValue: EntityType.track.rawValue) ?? EntityType.track, model: track as Any, view: self)
    }
    
    @IBAction func playBtn(_ sender: Any) {
        !isPlaying ? play() : pause()
    }
    
    @IBAction func nextBtn(_ sender: Any) {
        next()
    }
    
    @IBAction func previousBtn(_ sender: Any) {
        previous()
    }
}

// MARK: - Internal Logic
private extension MediaPlayerViewController {
    func setUp() {
        trackImage.maskWith(image: self.trackImage)
        favouritesBtn.isEnabled = isFavouriteEnabled
        containerView.center = self.view.center
        trackLabel.text = track.title
        loadTrack(previewUrl: track.preview)
        toggleButtons()
        updateImage()
    }
    
    func loadTrack(previewUrl: String) {
        guard let url = URL.init(string: previewUrl) else { return }
        let playerItem: AVPlayerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        play()
    }
    
    func play() {
        guard let player = player else { return }
        playBtn.setImage(UIImage(named: Constants.ImagePath.MediaPlayerIcon.play), for: .normal)
        player.play()
        isPlaying = true
    }
    
    func pause() {
        guard let player = player else { return }
        playBtn.setImage(UIImage(named: Constants.ImagePath.MediaPlayerIcon.stop), for: .normal)
        player.pause()
        isPlaying = false
    }
    
    func previous() {
        index -= 1
        guard !(index == 0) else {
            updatePlayerUI()
            return
        }
        updatePlayerUI()
    }
    
    func next() {
        index += 1
        guard index != tracks.count-1 else {
            updatePlayerUI()
            return
        }
        updatePlayerUI()
    }
    
    func setNextTrack(track: Track) {
        self.track = track
        trackLabel.text = self.track.title
    }
    
    func toggleButtons() {
        if index == tracks.count-1 { nextBtn.isEnabled = false } else { nextBtn.isEnabled = true }
        if index == 0 { previousBtn.isEnabled = false } else { previousBtn.isEnabled = true }
    }
    
    func updatePlayerUI() {
        toggleButtons()
        setNextTrack(track: tracks[index])
        loadTrack(previewUrl: tracks[index].preview)
    }
}

extension MediaPlayerViewController {
    func updateImage() {
        let imageWorker = ImageWorker(with: imageUrl)
        trackImage.image = imageWorker.loadImage()
        trackImage.contentMode = .scaleAspectFill
    }
    
    func updateMedia(index: Int, tracks: [Track], imageUrl: String, isFavouriteEnabled: Bool? = true) {
        track = tracks[index]
        self.index = index
        self.tracks = tracks
        self.imageUrl = imageUrl
        guard let isEnabled = isFavouriteEnabled else { return }
        self.isFavouriteEnabled = isEnabled
    }
}
