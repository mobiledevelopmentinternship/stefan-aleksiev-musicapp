//
//  AlbumViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 8.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class AlbumViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicatorImageView: UIImageView!
    
    // MARK: - Properties
    private var albumDataSource: TableViewAdapter!
    private var albums = [Album]()
    private var filteredAlbums = [Album]()
    private let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - Workers
    private var artistTopAlbums = ArtistTopAlbumsWorker(service: APIService())
    
    init(artistTopAlbums: ArtistTopAlbumsWorker) {
        self.artistTopAlbums = artistTopAlbums
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        artistTopAlbumsRequest()
        setUp()
    }
}

private extension AlbumViewController {
    func artistTopAlbumsRequest() {
        AppManager.shared.dispatchGroup.enter()
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView, view: self.view)
        artistTopAlbums.request(onSuccessCompletionHandler: { [weak self] albums in
            guard let self = self else { return }
            if albums.isEmpty {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.albums = albums
            }
            self.reloadData()
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func reloadData() {
        AppManager.shared.dispatchGroup.notify(queue: .main) {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
            
            self.setDataSource()
            self.tableView.reloadData()
        }
    }
    
    func stopIndicator() {
        DispatchQueue.main.async {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
        }
    }
}

// MARK: - Internal Logic
private extension AlbumViewController {
    func setUp() {
        activityIndicatorImageView.center = self.view.center
        setUpTableView()
        setBackButton(color: .white)
        setNavigationItems()
        searchBarSetup()
    }
    
    func searchBarSetup() {
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.placeholder = Constants.SearchBar.albumPlaceholder
        searchController.searchBar.barStyle = .default
        searchController.searchBar.setBackground()
        searchController.searchBar.subviews[0].subviews.compactMap(){ $0 as? UITextField }.first?.tintColor = .gray
        
        navigationItem.searchController = searchController
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController?.searchBar.tintColor = .white
    }
    
    func setUpTableView() {
        tableView.register(UINib(nibName: AlbumTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: AlbumTableViewCell.reuseIdentifier)
         tableView.register(UINib(nibName: SectionHeaderView.reuseIdentifier, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: SectionHeaderView.reuseIdentifier)
        tableView.separatorStyle = .none
        
        albumDataSource = TableViewAdapter()
        albumDataSource.setSectionViewHeader(with: Constants.SectionHeaderTitles.albums)
        albumDataSource.delegate = self
        
        tableView.dataSource = albumDataSource
        tableView.delegate = albumDataSource
        
        setDataSource()
    }
    
    func setDataSource() {
        albumDataSource.updateAlbumsArray(with: self.albums)
    }
    
    func setNavigationItems() {
        navigationItem.rightBarButtonItems = [
            super.favouriteNavigationItem]
    }
}

extension AlbumViewController {
    func updateAlbums(albums: [Album]) {
        self.albums = albums
    }
}

extension AlbumViewController: TableViewDataSourceAdapterDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        guard let url = URL(string: albums[indexPath.row].tracklist) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingAlbumError)
            return
        }
        let albumDetailsVC: AlbumDetailsViewController = self.instantiateViewController(with: AlbumDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        albumDetailsVC.updateAlbum(self.albums[indexPath.row])
        self.navigationController?.pushViewController(albumDetailsVC, animated: true)
    }
}

extension AlbumViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView,view: self.view)
        if searchText.isEmpty {
            albumDataSource.updateAlbumsArray(with: albums)
            albumDataSource.setItems(numberOfItemsInSection: albums.count)
        } else {
            filteredAlbums = albums
            self.filteredAlbums = self.albums.filter({ album-> Bool in
                guard let text = searchBar.text else { return false }
                return album.title.lowercased().contains(text.lowercased())
            })
            albumDataSource.updateAlbumsArray(with: filteredAlbums)
            albumDataSource.setItems(numberOfItemsInSection: filteredAlbums.count)
        }
        tableView.reloadData()
        stopIndicator()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView,view: self.view)
        guard let searchText = searchBar.text else { return }
        if !searchText.isEmpty {
            albumDataSource.updateAlbumsArray(with: albums)
            albumDataSource.setItems(numberOfItemsInSection: albums.count)
        }
        stopIndicator()
    }
}
