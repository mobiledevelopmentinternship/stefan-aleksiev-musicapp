//
//  ArtistDetailsViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 1.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class ArtistDetailsViewController: BaseViewController, AddButtonDelegate {
    
    // MARK: - Outlets
    @IBOutlet private weak var artistNameLabel: UILabel!
    @IBOutlet private weak var artistImageView: UIImageView!
    @IBOutlet private weak var trackCollectionView: UICollectionView!
    @IBOutlet private weak var albumCollectionView: UICollectionView!
    @IBOutlet private weak var recommendedCollectionView: UICollectionView!
    @IBOutlet private weak var activityIndicatorImageView: UIImageView!
    
    // MARK: - Titles only
    @IBOutlet private weak var popularSongsLabel: UILabel!
    @IBOutlet private weak var seeAllSongsBtn: UIButton!
    @IBOutlet private weak var recentAlbumsLabel: UILabel!
    @IBOutlet private weak var seeAllAlbumsBtn: UIButton!
    @IBOutlet private weak var recommendedArtistsLabel: UILabel!
    
    // MARK: - Properties
    private var trackDataSource,albumDataSource,recommendedDataSource: CollectionViewAdapter!
    private var artist: Artist!
    private var tracks = [Track]()
    private var albums = [Album]()
    private var recommendedArtists = [Artist]()
    
    // MARK: - Workers
    private var artistTopTracks = ArtistTopTracksWorker(service: APIService())
    private var artistTopAlbums = ArtistTopAlbumsWorker(service: APIService())
    private var coreDataStack = CoreDataStack()
    
    init(artistTopTracks: ArtistTopTracksWorker, artistTopAlbums: ArtistTopAlbumsWorker, coreDataStack: CoreDataStack) {
        self.artistTopTracks = artistTopTracks
        self.artistTopAlbums = artistTopAlbums
        self.coreDataStack = coreDataStack
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        artistTopTracksRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton(color: .appPurple)
    }
    
    override func addBtnPressed(_ sender: UIBarButtonItem) {
        guard let type = EntityType(rawValue: EntityType.artist.rawValue) else {
            self.showAlert(title: Constants.saveMessageResponse.saveTitleResponse.failure, message: Constants.saveMessageResponse.failure)
            return
        }
        guard let artistModel = artist else {
            self.showAlert(title: Constants.saveMessageResponse.saveTitleResponse.failure, message: Constants.saveMessageResponse.failure)
            return
        }
        coreDataStack.saveEntity(entityType: type, model: artistModel as Any, view: self)
    }
}

// MARK: - Requests
extension ArtistDetailsViewController {
    func artistTopTracksRequest() {
        AppManager.shared.dispatchGroup.enter()
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView, view: self.view)
        artistTopTracks.request(onSuccessCompletionHandler: { [weak self] tracks in
            guard let self = self else { return }
            if tracks.isEmpty {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.tracks = tracks
                self.artistTopAlbumsRequest()
            }
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func artistTopAlbumsRequest() {
        AppManager.shared.dispatchGroup.enter()
        artistTopAlbums.request(onSuccessCompletionHandler: { [weak self] albums in
            guard let self = self else { return }
            if albums.isEmpty {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.albums = albums
            }
            self.reloadData()
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func reloadData() {
        AppManager.shared.dispatchGroup.notify(queue: .main) {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
            
            self.setDataSource()
            self.trackCollectionView.reloadData()
            self.albumCollectionView.reloadData()
            self.recommendedCollectionView.reloadData()
            self.setTitles()
        }
    }
    
    func stopIndicator() {
        DispatchQueue.main.async {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
        }
    }
}

// MARK: - Actions
extension ArtistDetailsViewController {
    @IBAction private func seeAllBtn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            navigateToSongSearch()
        case 1:
            navigateToAlbumSearch()
        default:
            break
        }
    }
}

// MARK - Set-Up
private extension ArtistDetailsViewController {
    func setUp() {
        artistImageView.maskWith(image: self.artistImageView)
        activityIndicatorImageView.center = self.view.center
        setCollectionView()
        setNavigationItems()
    }
    
    func setCollectionView() {
        trackDataSource = CollectionViewAdapter(cellReuseIdentifier: TrackCollectionViewCell.reuseIdentifier)
        trackDataSource.delegate = self
        
        albumDataSource = CollectionViewAdapter(cellReuseIdentifier: AlbumCollectionViewCell.reuseIdentifier)
        albumDataSource.delegate = self
        
        recommendedDataSource = CollectionViewAdapter(cellReuseIdentifier: RecommendedCollectionViewCell.reuseIdentifier)
        recommendedDataSource.delegate = self
        
        trackCollectionView.dataSource = trackDataSource
        trackCollectionView.delegate = trackDataSource
        
        albumCollectionView.dataSource = albumDataSource
        albumCollectionView.delegate = albumDataSource
        
        recommendedCollectionView.dataSource = recommendedDataSource
        recommendedCollectionView.delegate = recommendedDataSource
    }
    
    func setDataSource() {
        self.artistNameLabel.text = self.artist.title
        updateImage(with: artist)
        
        trackDataSource.updateTrackArray(with: tracks)
        trackDataSource.setItems(numberOfItemsInSection: tracks.count)
        trackDataSource.updateTrackImage(with: artist)
        
        albumDataSource.setItems(numberOfItemsInSection: albums.count)
        albumDataSource.updateAlbumsArray(with: albums)
        
        recommendedArtists = AppManager.shared.recommendedArtists.choose(5)
        recommendedDataSource.setItems(numberOfItemsInSection: recommendedArtists.count)
        recommendedDataSource.updateRecommendedArtists(with: recommendedArtists)
    }
    
    func updateImage(with artist: Artist) {
        let imageWorker = ImageWorker(with: artist.picture)
        
        self.artistImageView.image = imageWorker.loadImage()
        self.artistImageView.contentMode = .scaleAspectFill
    }
    
    func setTitles() {
        popularSongsLabel.text = Constants.Titles.popularSongsLabel
        recommendedArtistsLabel.text = Constants.Titles.recommendedArtistsLabel
        recentAlbumsLabel.text = Constants.Titles.recentAlbumsLabel
        seeAllSongsBtn.setTitle(Constants.Titles.seeAllBtnLabel, for: .normal)
        seeAllAlbumsBtn.setTitle(Constants.Titles.seeAllBtnLabel, for: .normal)
    }
    
    func setNavigationItems() {
        navigationItem.rightBarButtonItems = [
            super.favouriteNavigationItem,
            super.addNavigationItem]
    }
}

extension ArtistDetailsViewController: CollectionViewDataSourceAdapterDelegate {
    func collectionView(_ collectionView: UICollectionView, didSeselectItemAt indexPath: IndexPath) {
        switch collectionView {
        case trackCollectionView:
            presentMediaPlayer(indexPath: indexPath)
        case albumCollectionView:
            self.navigateToAlbum(indexPath: indexPath)
        case recommendedCollectionView:
            self.navigateToArtist(indexPath: indexPath)
        default:
            break
        }
    }
    
    func navigateToAlbum(indexPath: IndexPath) {
        guard let url = URL(string: albums[indexPath.row].tracklist) else { return }
        let albumDetailsVC: AlbumDetailsViewController = self.instantiateViewController(with: AlbumDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        albumDetailsVC.updateAlbum(self.albums[indexPath.row])
        self.navigationController?.pushViewController(albumDetailsVC, animated: true)
    }
    
    func navigateToSongSearch() {
        guard let url = URL(string: artist.tracklist) else { return }
        let songVC: SongViewController = self.instantiateViewController(with: SongViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        songVC.updateArtist(artist: artist)
        self.navigationController?.pushViewController(songVC, animated: true)
    }
    
    func navigateToArtist(indexPath: IndexPath) {
        guard let url = URL(string: recommendedArtists[indexPath.row].tracklist) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingArtistError)
            return
        }
        let artistDetailsVC: ArtistDetailsViewController = self.instantiateViewController(with: ArtistDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        artistDetailsVC.updateArtist(artist: self.recommendedArtists[indexPath.row])
        self.navigationController?.pushViewController(artistDetailsVC, animated: true)
    }
    
    func navigateToAlbumSearch() {
        guard let url = URL(string: artist.tracklist) else { return }
        let albumVC: AlbumViewController = self.instantiateViewController(with: AlbumViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        self.navigationController?.pushViewController(albumVC, animated: true)
    }
    
    private func presentMediaPlayer(indexPath: IndexPath) {
        self.view.addSubview(ActivityIndicatorManager.shared.transparentView)
        let mediaPlayer: MediaPlayerViewController = self.instantiateViewController(with: MediaPlayerViewController.reuseIdentifier)
        mediaPlayer.updateMedia(index: indexPath.row, tracks: tracks, imageUrl: artist.picture)
        self.present(mediaPlayer, animated: true)
    }
    
    func updateArtist(artist: Artist) {
        self.artist = artist
    }
}
