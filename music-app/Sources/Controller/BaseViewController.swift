//
//  BaseViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 4.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    weak var delegate: AddButtonDelegate?
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    lazy var favouriteNavigationItem: UIBarButtonItem = {
        let button = UIButton(type: .custom)
        button.frame = Constants.NavigationItem.size
        
        let buttonImage = UIImage(named: Constants.ImagePath.favouriteIcon)
        button.setImage(buttonImage, for: .normal)
        button.addTarget(self, action: #selector(onFavouriteBtnPressed(_:)), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }()
    
    lazy var addNavigationItem: UIBarButtonItem = {
        let button = UIButton(type: .custom)
        button.frame = Constants.NavigationItem.size
        
        let buttonImage = UIImage(named: Constants.ImagePath.plusIcon)
        button.setImage(buttonImage, for: .normal)
        button.addTarget(self, action: #selector(addBtnPressed(_:)), for: .touchUpInside)
        
        return UIBarButtonItem(customView: button)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackButton()
    }
    
    @objc func addBtnPressed(_ sender: UIBarButtonItem) {
        delegate?.addBtnPressed(sender)
    }
}

// MARK: - Set-up
private extension BaseViewController {
    @objc func onFavouriteBtnPressed(_ sender: UIBarButtonItem) {
        let favouritesVC: FavouritesViewController = self.instantiateViewController(with: FavouritesViewController.reuseIdentifier)
        self.navigationController?.pushViewController(favouritesVC, animated: true)
    }
}

extension BaseViewController {
    func setBackButton(color: UIColor) {
        navigationController?.navigationBar.tintColor = color
    }
    
    func setBackground() {
        let backgroundImage = UIImageView()
        view.addSubview(backgroundImage)
        
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        
        backgroundImage.image = UIImage(named: Constants.ImagePath.defaultBackground)
        backgroundImage.contentMode = .scaleAspectFill
        
        self.view.insertSubview(backgroundImage, at: 0)
    }
    
    func setTableViewHeight(count: Int, tableView: UITableView) {
        let totalHeight = (CGFloat(count) * tableView.rowHeight)
        if totalHeight < tableView.frame.height {
            tableView.isScrollEnabled = false
        }
        tableView.frame = CGRect(x:0, y:0, width:tableView.frame.width, height:totalHeight)
    }
    
    func setBackButton() {
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: Constants.ImagePath.backIcon)
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: Constants.ImagePath.backIcon)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

protocol ControllerTitle {
    var navigationTitle: String { get }
}

protocol AddButtonDelegate: class {
    func addBtnPressed(_ sender: UIBarButtonItem)
}
