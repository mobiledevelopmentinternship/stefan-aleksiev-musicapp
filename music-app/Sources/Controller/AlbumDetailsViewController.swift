//
//  AlbumDetailsViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 8.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class AlbumDetailsViewController: BaseViewController, AddButtonDelegate {
    
    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var activityIndicatorImageView: UIImageView!
    
    // MARK: - Properties
    private var album: Album!
    private var playlist: Playlist!
    private var model: Any!
    private var tracks = [Track]()
    private var trackDataSource: TableViewAdapter!
    private var type = ""
    private var entity = ""
    
    // MARK: - Workers
    private var artistTopTracks = ArtistTopTracksWorker(service: APIService())
    private var coreDataStack = CoreDataStack()
    
    init(artistTopTracks: ArtistTopTracksWorker, coreDataStack: CoreDataStack) {
        self.artistTopTracks = artistTopTracks
        self.coreDataStack = coreDataStack
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        albumTracklistRequest()
        setUp()
    }
    
    override func addBtnPressed(_ sender: UIBarButtonItem) {
        switch type {
        case ObjectType.album.rawValue:
            guard let albumModel = album else {
                self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingAlbumError)
                return
            }
            entity = EntityType.album.rawValue
            model = albumModel
        case ObjectType.playlist.rawValue:
            guard let playlistModel = playlist else {
                self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingPlaylistError)
                return
            }
            entity = EntityType.playlist.rawValue
            model = playlistModel
        default:
            break
        }
            coreDataStack.saveEntity(entityType: EntityType(rawValue: entity) ?? EntityType.album, model: model as Any, view: self)
    }
}

// MARK: - Requests
private extension AlbumDetailsViewController {
    func albumTracklistRequest() {
        AppManager.shared.dispatchGroup.enter()
        ActivityIndicatorManager.shared.startIndicator(imageView: activityIndicatorImageView, view: self.view)
        artistTopTracks.request(onSuccessCompletionHandler: { [weak self] tracks in
            guard let self = self else { return }
            if tracks.isEmpty {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.tracks = tracks
            }
            self.reloadData()
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func reloadData() {
        AppManager.shared.dispatchGroup.notify(queue: .main) {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
            
            self.setDataSource(type: ObjectType(rawValue: self.type) ?? ObjectType.album)
            super.setTableViewHeight(count: self.tracks.count, tableView: self.tableView)
            self.tableView.reloadData()
        }
    }
    
    func stopIndicator() {
        DispatchQueue.main.async {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
        }
    }
}

// MARK: - Internal Logic
extension AlbumDetailsViewController: DataTypeable {
    private func setUp() {
        self.activityIndicatorImageView.center = self.view.center
        setUpTableView()
        setBackButton(color: .white)
        setNavigationItems()
    }
    
    private func setUpTableView() {
        tableView.register(UINib(nibName: TrackTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: TrackTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: SectionHeaderView.reuseIdentifier, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: SectionHeaderView.reuseIdentifier)
        tableView.separatorStyle = .none
        
        trackDataSource = TableViewAdapter()
        trackDataSource.delegate = self
        trackDataSource.setSectionViewHeader(with: Constants.SectionHeaderTitles.tracks)
        
        tableView.dataSource = trackDataSource
        tableView.delegate = trackDataSource
    }
    
    func setDataSource(type: ObjectType) {
        switch type {
        case .album:
            setObjects(label: album.title, image: album.picture)
            trackDataSource.updateAlbum(with: album)
        case .playlist:
            setObjects(label: playlist.title, image: playlist.picture)
            trackDataSource.updatePlaylist(with: playlist)
        default:
            self.showAlert(title: RequestError.errorTitle, message: "No such type as \(type)")
        }
    }
    
    private func setObjects(label: String,image: String) {
        self.label.text = label
        updateImage(with: image)
        trackDataSource.updateTracksArray(with: tracks)
    }
    
    private func updateImage(with image: String) {
        let imageWorker = ImageWorker(with: image)
        imageView.image = imageWorker.loadImage()
        imageView.contentMode = .scaleAspectFill
    }
    
    func setNavigationItems() {
        navigationItem.rightBarButtonItems = [
            super.favouriteNavigationItem,
            super.addNavigationItem]
    }
}

// MARK: - Update Functions
extension AlbumDetailsViewController {
    func updateAlbum(_ album: Album) {
        self.type = album.type
        self.album = album
    }
    
    func updatePlaylist(playlist: Playlist) {
        self.type = playlist.type
        self.playlist = playlist
    }
}

// MARK: - Delegates
extension AlbumDetailsViewController: TableViewDataSourceAdapterDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openMedia(indexPath: indexPath)
    }
    
    func openMedia(indexPath: IndexPath) {
        self.view.addSubview(ActivityIndicatorManager.shared.transparentView)
        let mediaPlayer: MediaPlayerViewController = self.instantiateViewController(with: MediaPlayerViewController.reuseIdentifier)
        var picture = ""
        switch type {
        case ObjectType.album.rawValue:
            picture = album.picture
        case ObjectType.playlist.rawValue:
            picture = playlist.picture
        default:
            break
        }
        mediaPlayer.updateMedia(index: indexPath.row, tracks: tracks, imageUrl: picture)
        self.present(mediaPlayer, animated: true)
    }
}
