//
//  SongViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 8.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class SongViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicatorImageView: UIImageView!
    
    // MARK: - Properties
    private var trackDataSource: TableViewAdapter!
    private var artist: Artist!
    private var tracks = [Track]()
    private var filteredTracks = [Track]()
    private let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - Workers
    private var artistTopTracks = ArtistTopTracksWorker(service: APIService())
    
    init(artistTopTracks: ArtistTopTracksWorker) {
        self.artistTopTracks = artistTopTracks
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        artistTopTracksRequest()
        setUp()
    }
}

// MARK: - Requests
extension SongViewController {
    func artistTopTracksRequest() {
        AppManager.shared.dispatchGroup.enter()
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView, view: self.view)
        artistTopTracks.request(onSuccessCompletionHandler: { [weak self] tracks in
            guard let self = self else { return }
            if tracks.isEmpty {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.tracks = tracks
            }
            self.reloadData()
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func reloadData() {
        AppManager.shared.dispatchGroup.notify(queue: .main) {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
            
            self.setDataSource()
            self.tableView.reloadData()
        }
    }
    
    func stopIndicator() {
        DispatchQueue.main.async {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
        }
    }
}

// MARK: - Internal Logic
private extension SongViewController {
    func setUp() {
        activityIndicatorImageView.center = self.view.center
        setUpTableView()
        setBackButton(color: .white)
        setNavigationItems()
        searchBarSetup()
    }
    
    func searchBarSetup() {
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.placeholder = Constants.SearchBar.songPlaceholder
        searchController.searchBar.barStyle = .default
        searchController.searchBar.setBackground()
        searchController.searchBar.subviews[0].subviews.compactMap(){ $0 as? UITextField }.first?.tintColor = .gray
        
        navigationItem.searchController = searchController
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController?.searchBar.tintColor = .white
    }
    
    func setUpTableView() {
        tableView.register(UINib(nibName: TrackTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: TrackTableViewCell.reuseIdentifier)
        tableView.register(UINib(nibName: SectionHeaderView.reuseIdentifier, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: SectionHeaderView.reuseIdentifier)
        tableView.separatorStyle = .none
        
        trackDataSource = TableViewAdapter()
        trackDataSource.delegate = self
        trackDataSource.setSectionViewHeader(with: Constants.SectionHeaderTitles.tracks)
        
        
        tableView.dataSource = trackDataSource
        tableView.delegate = trackDataSource
    }
    
    func setDataSource() {
        trackDataSource.updateTracksArray(with: tracks)
        trackDataSource.updateArtist(with: artist)
        trackDataSource.setItems(numberOfItemsInSection: tracks.count)
    }
    
    func setNavigationItems() {
        navigationItem.rightBarButtonItems = [
            super.favouriteNavigationItem]
    }
    
    func presentMediaPlayer(indexPath: IndexPath) {
        self.view.addSubview(ActivityIndicatorManager.shared.transparentView)
        let mediaPlayer: MediaPlayerViewController = self.instantiateViewController(with: MediaPlayerViewController.reuseIdentifier)
        mediaPlayer.updateMedia(index: indexPath.row, tracks: tracks, imageUrl: artist.picture)
        self.present(mediaPlayer, animated: true)
    }
}

extension SongViewController {
    func updateArtist(artist: Artist) {
        self.artist = artist
    }
}

extension SongViewController: TableViewDataSourceAdapterDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presentMediaPlayer(indexPath: indexPath)
    }
}

extension SongViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView,view: self.view)
        if searchText.isEmpty {
            trackDataSource.updateTracksArray(with: tracks)
            trackDataSource.setItems(numberOfItemsInSection: tracks.count)
        } else {
            filteredTracks = tracks
            self.filteredTracks = self.tracks.filter({ track -> Bool in
                guard let text = searchBar.text else { return false }
                return track.title.lowercased().contains(text.lowercased())
            })
            trackDataSource.updateTracksArray(with: filteredTracks)
            trackDataSource.setItems(numberOfItemsInSection: filteredTracks.count)
        }
        tableView.reloadData()
        stopIndicator()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView,view: self.view)
        guard let searchText = searchBar.text else { return }
        if !searchText.isEmpty {
            trackDataSource.updateTracksArray(with: tracks)
            trackDataSource.setItems(numberOfItemsInSection: tracks.count)
        }
        stopIndicator()
    }
}
