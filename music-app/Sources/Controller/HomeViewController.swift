//
//  ViewController.swift
//  music-app
//
//  Created by Stefan Aleksiev on 22.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController, ControllerTitle {
    
    // MARK: - Outlets
    @IBOutlet private weak var artistCollectionView: UICollectionView!
    @IBOutlet private weak var albumCollectionView: UICollectionView!
    @IBOutlet private weak var playlistImageView: UIImageView!
    @IBOutlet private weak var activityIndicatorImageView: UIImageView!
    @IBOutlet private weak var playlistImageViewContainer: UIView!
    
    // MARK: - Titles only
    @IBOutlet private weak var top10artistsLabel: UILabel!
    @IBOutlet private weak var top10albumsLabel: UILabel!
    @IBOutlet private weak var seeAllBtnLabel: UIButton!
    
    // MARK: - Properties
    private var artistDataSource: CollectionViewAdapter!
    private var albumDataSource: CollectionViewAdapter!
    private var artists = [Artist]()
    private var albums = [Album]()
    private var tracks = [Track]()
    private var playlist: Playlist!
    var navigationTitle: String {
        return Constants.NavigationTitles.homeTitle
    }
    
    // MARK: - Workers
    private var chartPlaylistsWorker = ChartPlaylistWorker(service: APIService())
    private var chartArtistsWorker = ChartArtistWorker(service: APIService())
    private var chartAlbumsWorker = ChartAlbumWorker(service: APIService())
    
    init(chartAlbumsWorker: ChartAlbumWorker, chartArtistsWorker: ChartArtistWorker, chartPlaylistsWorker: ChartPlaylistWorker) {
        self.chartAlbumsWorker = chartAlbumsWorker
        self.chartArtistsWorker = chartArtistsWorker
        self.chartPlaylistsWorker = chartPlaylistsWorker
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicatorManager.shared.createIndicator(imageView: self.activityIndicatorImageView, view: self.view)
        requestChartArtists()
        setUp()
    }
}

// MARK: - Requests
private extension HomeViewController {
    func requestChartArtists() {
        AppManager.shared.dispatchGroup.enter()
        ActivityIndicatorManager.shared.startIndicator(imageView: self.activityIndicatorImageView, view: self.view)
        chartArtistsWorker.request(onSuccessCompletionHandler: { [weak self] artists in
            guard let self = self else { return }
            if artists.isEmpty {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.artists = artists
                self.chartAlbumsRequest()
            }
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func chartAlbumsRequest() {
        AppManager.shared.dispatchGroup.enter()
        chartAlbumsWorker.request(onSuccessCompletionHandler: { [weak self] albums in
            guard let self = self else { return }
            if albums.isEmpty {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.albums = albums
                self.chartPlaylisRequest()
            }
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func chartPlaylisRequest() {
        AppManager.shared.dispatchGroup.enter()
        chartPlaylistsWorker.request(onSuccessCompletionHandler: { [weak self] playlist in
            guard let self = self else { return }
            if playlist.title == "" {
                AppManager.shared.redirectBack(image: self.activityIndicatorImageView, viewController: self)
            } else {
                self.playlist = playlist
            }
            self.reloadData()
            }, onErrorHandler: { [weak self] error in
                guard let self = self else { return }
                self.showAlert(title: RequestError.errorTitle, message: error.localizedDescription)
                self.stopIndicator()
        })
        AppManager.shared.dispatchGroup.leave()
    }
    
    func reloadData() {
        AppManager.shared.dispatchGroup.notify(queue: .main) {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
            
            self.setDataSource()
            self.updateImage(with: self.playlist)
            self.artistCollectionView.reloadData()
            self.albumCollectionView.reloadData()
            self.setTitles()
        }
    }
    
    func stopIndicator() {
        DispatchQueue.main.async {
            ActivityIndicatorManager.shared.stopIndicator(imageView: self.activityIndicatorImageView)
            ActivityIndicatorManager.shared.removeTransparentView(view: self.view)
        }
    }
}

// MARK: - Actions
private extension HomeViewController {
    @IBAction func seeAllBtn(_ sender: Any) {
        navigateToArtistSearch()
    }
}

// MARK: - Set-up
private extension HomeViewController {
    func setUp() {
        activityIndicatorImageView.center = self.view.center
        title = navigationTitle
        setNavigationItems()
        setCollectionView()
        addPlaylistTapGesture()
    }
    
    func addPlaylistTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(navigateToPlaylist))
        playlistImageView.addGestureRecognizer(tap)
        playlistImageView.isUserInteractionEnabled = true
    }
    
    func setCollectionView() {
        artistDataSource = CollectionViewAdapter(cellReuseIdentifier: ArtistCollectionViewCell.reuseIdentifier)
        artistDataSource.delegate = self
        
        albumDataSource = CollectionViewAdapter(cellReuseIdentifier: AlbumCollectionViewCell.reuseIdentifier)
        albumDataSource.delegate = self
        
        artistCollectionView.dataSource = artistDataSource
        artistCollectionView.delegate = artistDataSource
        
        albumCollectionView.dataSource = albumDataSource
        albumCollectionView.delegate = albumDataSource
    }
    
    func setDataSource() {
        artistDataSource.setItems(numberOfItemsInSection: artists.count)
        artistDataSource.updateArtistArray(with: artists)
        
        albumDataSource.setItems(numberOfItemsInSection: albums.count)
        albumDataSource.updateAlbumsArray(with: albums)
        
        AppManager.shared.setRecommendedArtists(artists: artists)
    }
    
    func updateImage(with playlist: Playlist) {
        let imageWorker = ImageWorker(with: playlist.picture)
        playlistImageView.image = imageWorker.loadImage()
    }
    
    func setTitles() {
        top10artistsLabel.text = Constants.Titles.top10artistsLabel
        top10albumsLabel.text = Constants.Titles.top10albumsLabel
        seeAllBtnLabel.setTitle(Constants.Titles.seeAllBtnLabel, for: .normal)
    }
    
    func setNavigationItems() {
        navigationItem.rightBarButtonItems = [
            super.favouriteNavigationItem]
    }
}

// MARK: - DataSourceAdapterDelegate Functions
extension HomeViewController: CollectionViewDataSourceAdapterDelegate {
    func collectionView(_ collectionView: UICollectionView, didSeselectItemAt indexPath: IndexPath) {
        switch collectionView {
        case artistCollectionView:
            self.navigateToArtist(indexPath: indexPath)
        case albumCollectionView:
            self.navigateToAlbum(indexPath: indexPath)
        default:
            break
            // TODO: -- Handle the tap and ignore it. --
        }
    }
    
    func navigateToArtist(indexPath: IndexPath) {
        guard let url = URL(string: artists[indexPath.row].tracklist) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingArtistError)
            return
        }
        let artistDetailsVC: ArtistDetailsViewController = self.instantiateViewController(with: ArtistDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        artistDetailsVC.updateArtist(artist: self.artists[indexPath.row])
        self.navigationController?.pushViewController(artistDetailsVC, animated: true)
    }
    
    func navigateToAlbum(indexPath: IndexPath) {
        guard let url = URL(string: albums[indexPath.row].tracklist) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingAlbumError)
            return
        }
        let albumDetailsVC: AlbumDetailsViewController = self.instantiateViewController(with: AlbumDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        albumDetailsVC.updateAlbum(self.albums[indexPath.row])
        self.navigationController?.pushViewController(albumDetailsVC, animated: true)
    }
    
    @objc func navigateToPlaylist() {
        guard let url = URL(string: playlist.trackList) else {
            self.showAlert(title: RequestError.errorTitle, message: RequestError.fetchingPlaylistError)
            return
        }
        let albumDetailsVC: AlbumDetailsViewController = self.instantiateViewController(with: AlbumDetailsViewController.reuseIdentifier)
        AppManager.shared.setAlbumTracklist(url: url)
        albumDetailsVC.updatePlaylist(playlist: playlist)
        self.navigationController?.pushViewController(albumDetailsVC, animated: true)
    }
    
    func navigateToArtistSearch() {
        let artistSearch: ArtistViewController = self.instantiateViewController(with: ArtistViewController.reuseIdentifier)
        artistSearch.updateArtists(artists: artists)
        self.navigationController?.pushViewController(artistSearch, animated: true)
    }
}
