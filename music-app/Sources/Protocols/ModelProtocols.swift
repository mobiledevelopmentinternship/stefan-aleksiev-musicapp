//
//  ModelTypeProtocol.swift
//  music-app
//
//  Created by Stefan Aleksiev on 29.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

enum ObjectType: String {
    case album
    case artist
    case playlist
    case track
}
protocol ModelTypeable {
    var type: String { get set }
}

protocol ObjectTypeable {
    func getObjectType(type: ObjectType) -> String
}

protocol DataTypeable {
    func setDataSource(type: ObjectType) -> Void
}
