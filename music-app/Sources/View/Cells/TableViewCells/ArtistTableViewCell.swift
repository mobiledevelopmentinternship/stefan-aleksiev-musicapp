//
//  ArtistTableViewCell.swift
//  music-app
//
//  Created by Stefan Aleksiev on 24.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class ArtistTableViewCell: UITableViewCell, TableViewCellConfigurable {
    
    // MARK: - Outlets
    @IBOutlet private weak var artistImageView: UIImageView!
    @IBOutlet private weak var artistLabel: UILabel!
    
    func configure(with picturePath: String, with title: String) {
        let imageWorker = ImageWorker(with: picturePath)
        
        artistImageView.image = imageWorker.loadImage()
        artistImageView.contentMode = .scaleAspectFill
        artistLabel.text = title
    }
}

protocol TableViewCellConfigurable {
    func configure(with picturePath: String, with title: String)
}
