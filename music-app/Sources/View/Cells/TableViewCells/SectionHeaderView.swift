//
//  SectionHeaderCell.swift
//  music-app
//
//  Created by Stefan Aleksiev on 7.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class SectionHeaderView: UITableViewHeaderFooterView {
    
    // MARK: - Outlets
    @IBOutlet private weak var sectionTitle: UILabel!
    @IBOutlet private weak var sectionImage: UIImageView!
    
    func configure(with sectionTitle: String, with sectionImagePath: String) {
        self.sectionTitle.text = sectionTitle
        self.sectionImage.image = UIImage(named: sectionImagePath)
    }
}
