//
//  TrackTableViewCell.swift
//  music-app
//
//  Created by Stefan Aleksiev on 17.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class TrackTableViewCell: UITableViewCell, TableViewCellConfigurable {

    // MARK: - Outlets
    @IBOutlet private weak var trackImageView: UIImageView!
    @IBOutlet private weak var trackLabel: UILabel!
    
    func configure(with picturePath: String, with title: String) {
        trackImageView.image = UIImage(named: Constants.ImagePath.defaultTrack)
        trackImageView.contentMode = .scaleAspectFit
        trackLabel.text = title
    }
}
