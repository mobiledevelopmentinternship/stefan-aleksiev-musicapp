//
//  AlbumTableViewCell.swift
//  music-app
//
//  Created by Stefan Aleksiev on 3.06.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell, TableViewCellConfigurable {
    
    // MARK: - Outlets
    @IBOutlet private weak var albumImageView: UIImageView!
    @IBOutlet private weak var albumLabel: UILabel!
    
    func configure(with picturePath: String, with title: String) {
        let imageWorker = ImageWorker(with: picturePath)
        
        albumImageView.image = imageWorker.loadImage()
        albumImageView.contentMode = .scaleAspectFill
        albumLabel.text = title
    }
}
