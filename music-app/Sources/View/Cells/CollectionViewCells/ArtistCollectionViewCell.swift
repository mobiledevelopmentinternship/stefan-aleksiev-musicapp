//
//  ArtistCollectionViewCell.swift
//  music-app
//
//  Created by Stefan Aleksiev on 25.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class ArtistCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet private weak var artistImageView: UIImageView!

    func configure(with picturePath: String) {
        let imageWorker = ImageWorker(with: picturePath)
        
        self.artistImageView.image = imageWorker.loadImage()
        self.artistImageView.contentMode = .scaleAspectFill
    }
}
