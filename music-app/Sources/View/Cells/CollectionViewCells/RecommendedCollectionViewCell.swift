//
//  RecommendedCollectionViewCell.swift
//  music-app
//
//  Created by Stefan Aleksiev on 2.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class RecommendedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var recommendedImageView: UIImageView!
    
    func configure(with picturePath: String) {
        let imageWorker = ImageWorker(with: picturePath)
        self.recommendedImageView.image = imageWorker.loadImage()
        self.recommendedImageView.contentMode = .scaleAspectFill
    }
}
