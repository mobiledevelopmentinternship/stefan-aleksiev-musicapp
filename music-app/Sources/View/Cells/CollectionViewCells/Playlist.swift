//
//  Playlist.swift
//  music-app
//
//  Created by Stefan Aleksiev on 6.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct Playlist {
    
    let id: Int
    let title: String
    let numberOfTracks: Int
    //let picture: String
    //let trackList: [Track]
    let type: String
    
    init(_ dictionary: [String: Any]) {
        self.id = dictionary["id"] as? Int ?? 0
        self.title = dictionary["title"] as? String ?? ""
        self.numberOfTracks = dictionary["nb_tracks"] as? Int ?? 0
        self.type = dictionary["type"] as? String ?? ""
    }
}
