//
//  Track.swift
//  music-app
//
//  Created by Stefan Aleksiev on 6.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct Track {
    
    let title: String
    let duration: Int
    let preview: String
    let type: String
    
}
