//
//  Album.swift
//  music-app
//
//  Created by Stefan Aleksiev on 6.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct Album {
    
    let title: String
    //let cover: String
    let position: Int
    let type: String
    
    init(_ dictionary: [String: Any]) {
        self.title = dictionary["title"] as? String ?? ""
        self.position = dictionary["position"] as? Int ?? 0
        self.type = dictionary["type"] as? String ?? ""
    }
}
