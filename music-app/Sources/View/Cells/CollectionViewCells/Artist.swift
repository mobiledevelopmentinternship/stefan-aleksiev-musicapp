//
//  Artist.swift
//  music-app
//
//  Created by Stefan Aleksiev on 6.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import Foundation

struct Artist {
    
    let id:Int
    let name:String
    //let picture:String
    let position:Int
    //let fans:Int
    let type:String
    
    init(_ dictionary: [String: Any]) {
        self.id = dictionary["id"] as? Int ?? 0
        self.name = dictionary["name"] as? String ?? ""
        self.position = dictionary["position"] as? Int ?? 0
        self.type = dictionary["type"] as? String ?? ""
    }
}
