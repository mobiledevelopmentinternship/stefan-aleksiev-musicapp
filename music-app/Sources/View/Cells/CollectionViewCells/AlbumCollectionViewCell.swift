//
//  AlbumCollectionViewCell.swift
//  music-app
//
//  Created by Stefan Aleksiev on 25.04.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet private weak var albumImageView: UIImageView!
    
    func configure(with coverPath: String) {
        let imageWorker = ImageWorker(with: coverPath)
        
        self.albumImageView.image = imageWorker.loadImage()
        self.albumImageView.contentMode = .scaleAspectFill
    }
}

