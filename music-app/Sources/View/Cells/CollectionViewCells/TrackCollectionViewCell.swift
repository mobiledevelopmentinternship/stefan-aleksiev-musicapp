//
//  TrackCollectionViewCell.swift
//  music-app
//
//  Created by Stefan Aleksiev on 1.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class TrackCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet private weak var trackImageView: UIImageView!
    @IBOutlet private weak var trackLabel: UILabel!
    
    func configure(with picturePath: String, with title: String) {
        let imageWorker = ImageWorker(with: picturePath)
        
        self.trackImageView.image = imageWorker.loadImage()
        self.trackImageView.contentMode = .scaleAspectFill
        trackLabel.text = title
    }
}
