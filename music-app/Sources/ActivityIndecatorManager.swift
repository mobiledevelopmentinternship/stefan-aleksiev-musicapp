//
//  ActivityIndecatorManager.swift
//  music-app
//
//  Created by Stefan Aleksiev on 14.05.19.
//  Copyright © 2019 Stefan Aleksiev. All rights reserved.
//

import UIKit

class ActivityIndicatorManager : UIView {

    // MARK: - Singleton
    static let shared = ActivityIndicatorManager()
    private var indicatorImages: [UIImage] = []
    private var framesLenght = 29
    
    lazy var transparentView: UIView = {
        let transparentView = UIView(frame: UIScreen.main.bounds)
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        transparentView.isUserInteractionEnabled = false
        return transparentView
    }()
    
    func createIndicator(imageView: UIImageView, view: UIView!) {
        createImageArray(total: self.framesLenght, imagePrefix: "frame")
    }
    
    func startIndicator(imageView: UIImageView, view: UIView!) {
        performAnimation(imageView: imageView, images: indicatorImages,view: view)
    }
    
    func stopIndicator(imageView: UIImageView?) {
        imageView?.stopAnimating()
    }
    
    func removeTransparentView(view: UIView) {
        self.transparentView.removeFromSuperview()
        view.isUserInteractionEnabled = true
    }
    
    func createImageArray(total: Int, imagePrefix: String) {
        for imageCount in 0..<total {
            let imageName = "\(imagePrefix)-\(imageCount).png"
            guard let image = UIImage(named: imageName) else { continue }
            
            indicatorImages.append(image)
        }
    }
    
    func performAnimation(imageView: UIImageView, images: [UIImage], view: UIView!) {
            self.addSubview(self.transparentView)
            self.transparentView.addSubview(imageView)
            self.transparentView.bringSubviewToFront(imageView)
            UIApplication.shared.keyWindow?.addSubview(self.transparentView)
            
            imageView.animationImages = images
            imageView.animationDuration = 1
            imageView.animationRepeatCount = 2
            imageView.startAnimating()
    }
}
